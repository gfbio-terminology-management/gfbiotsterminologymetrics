package org.gfbio.terminologies.upload;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Set;
import org.apache.log4j.Logger;
import org.gfbio.config.ConfigSvc;
import org.gfbio.db.VirtGraphSingleton;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import com.hp.hpl.jena.util.FileUtils;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

public class GFBioOntologyUploader {

  private ConfigSvc configSvc = ConfigSvc.getInstance(null);

  private static final Logger LOGGER = Logger.getLogger(GFBioOntologyUploader.class);

  private String uriPrefix;

  private VirtGraph set;

  private boolean loadImports;

  /**
   * 
   * @param prefix Custom URI prefix
   * @param loadImports Whether imports should be loaded as well
   */
  public GFBioOntologyUploader(String prefix, boolean loadImports) {
    this.set = VirtGraphSingleton.getInstance().getVirtGraph();
    this.loadImports = loadImports;
    this.uriPrefix = prefix;
  }

  /**
   * Upload an ontology to into virtuoso
   * 
   * @param file path fo the file (r.g. /home/foo/bar.bco) ! Make sure that virtuoso can access this
   *        directory !
   * @param acronym acronym from the ontology, e.g., 'BCO' 'PATO'
   * @param suffix additional graph name suffix, e.g., 'OLD' 'NEW'
   */
  public int upload(String file, String acronym, String suffix) {
    try {
      String path = FileUtils.getDirname(file);
      // String name = FileUtils.getBasename(file);
      String ext = getExtensionByStringHandling(file).get();
      clearUploadTable(file);
      String fillTableQuery =
          "ld_dir ('" + path + "', '%." + ext + "', '" + uriPrefix + acronym + suffix + "')";
      String uploadQUery = "rdf_loader_run ()";

      executeSQL(fillTableQuery);
      executeSQL(uploadQUery);
      if (loadImports == true)
        return loadImports(file, acronym);
      else {
        LOGGER.info("Imports were not loaded");
        return 1;
      }
    } catch (OutOfMemoryError e) {
      LOGGER.error("Upload failed due to OutOfMemory " + e.getLocalizedMessage());
      return -1;
    }
  }

  /**
   * Clear all entries in DB.DBA.load_list for the given file to make it uploadable
   * 
   * @param file file path
   * @return
   */
  private boolean clearUploadTable(String file) {
    return executeSQL("DELETE FROM DB.DBA.load_list WHERE ll_file = '" + file + "'");
  }

  /**
   * Determines the file extension using the filename as input
   * 
   * @param filename
   * @return
   */
  private Optional<String> getExtensionByStringHandling(String filename) {
    return Optional.ofNullable(filename).filter(f -> f.contains("."))
        .map(f -> f.substring(filename.lastIndexOf(".") + 1));
  }

  /**
   * Execute an sql query
   * 
   * @param sql query
   * @return
   */
  private boolean executeSQL(String sql) {

    LOGGER.info("executing SQL query " + sql);

    boolean executed = false;
    try {
      Class.forName("virtuoso.jdbc4.Driver");
      Connection conn = DriverManager.getConnection(configSvc.getHostURL(), configSvc.getUsername(),
          configSvc.getPassword());
      PreparedStatement st;
      st = conn.prepareStatement(sql);
      executed = st.execute();
      st.close();
    } catch (ClassNotFoundException e) {
      LOGGER.error("Could not find class " + e.getLocalizedMessage());
    } catch (SQLException e) {
      LOGGER.error("Could not establish a connection due to " + e.getErrorCode() + " "
          + e.getLocalizedMessage());
    }
    return executed;
  }

  /**
   * This method loads the imports for a given ontology into Virtuoso.
   * 
   * @param ontology the local ontology filename
   * @param graphName name of the graph
   */
  private int loadImports(String ontology, String graphName) {
    ontology = "file:///" + ontology;
    String[] array = ontology.split("\\.");
    String fileExtension = array[1];
    // if (loadImports == true) {
    if (fileExtension.equals("owl") || fileExtension.equals("rdf")) {
      OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
      IRI iri = IRI.create(ontology);
      try {
        OWLOntology onto = manager.loadOntologyFromOntologyDocument(iri);
        Set<OWLOntology> imports = manager.getImports(onto);
        // for each import, fire a SPARQL query that uploads this import into Virtuoso
        for (OWLOntology importOnto : imports) {
          String sparql = "LOAD <" + importOnto.getOntologyID().getOntologyIRI().get()
              + "> INTO GRAPH <" + uriPrefix + graphName + ">";

          LOGGER.info("importing ontology query " + sparql);

          VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
          vqe.execSelect();
          vqe.close();
        }
      } catch (OWLOntologyCreationException e) {
        LOGGER.error("Could not load imports due to " + e.getLocalizedMessage());
        return -1;
      }
    } else {
      LOGGER.warn("Provided file does not end with .owl! Cannot process file/imports!");
      return 0;
    }
    return 1;
  }

  /**
   * Indexing of a newly added graph
   */
  public boolean index() {
    return executeSQL("DB.DBA.VT_INC_INDEX_DB_DBA_RDF_OBJ ()");
  }

}
