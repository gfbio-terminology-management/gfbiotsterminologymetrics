package org.gfbio.terminologies.metrics;

public abstract class GFBioMetricsCalculator {

  public abstract int getClassesCount();

  public abstract int getIndividualsCount();

  public abstract int getPropertiesCount();

  public abstract int getMaxDepth();

  public abstract int getMaxChildren();

  public abstract int getAvgChildren();

  public abstract int getSingleChild();

  public abstract int getMore25Children();

  public abstract int getClassesWithoutDefinition();

  public abstract int getClasesWithoutLabel();

  public abstract int getClassesWithMoreThan1Parent();

  public abstract int getNumberOfLeaves();

  public abstract String getDescriptionLogicName();

  public abstract void calculateMetrics();

  public abstract void setDefinition(String definitionProperty);

  public abstract void setLabel(String labelProperty);

  public abstract void setDescriptionLogicName(String descriptionLogicName);

  public abstract void setAcronym(String acronym);
}
