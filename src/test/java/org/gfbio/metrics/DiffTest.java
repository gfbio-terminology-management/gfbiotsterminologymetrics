package org.gfbio.metrics;

import static org.junit.Assert.assertEquals;
import org.gfbio.terminologies.configuration.GFBioConfiguration;
import org.gfbio.terminologies.metrics.SPARQLAdapter;
import org.gfbio.terminologies.xml.OntologyChangesBean;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class DiffTest {
  /*
   * tested with ENVO 2017-08-22 and ENVO 2020-06-10
   */
  private static final String acronym = "ENVO";

  private static SPARQLAdapter adapter;

  private static OntologyChangesBean bean;

  @BeforeClass
  public static void setup() {
    GFBioConfiguration config = GFBioConfiguration.getInstance();
    config.setOntologyAcronym(acronym);
    config.setReleaseDate("2020-07-31");
    config.initProperties();

    bean = new OntologyChangesBean();

    adapter = new SPARQLAdapter(bean);
  }

  @Test
  @Ignore
  public void testCountAdded() throws Exception {
    System.out.println("*** testCountAdded ***");
    adapter.countAdded("OLD", "");
    assertEquals(1654, bean.getNumNewClasses());
  }

  @Test
  @Ignore
  public void testCountDeleted() throws Exception {
    System.out.println("*** testCountDeleted ***");
    adapter.countDeleted("OLD", "");
    assertEquals(2364, bean.getNumDeletedClasses());
  }

  @Test
  @Ignore
  public void TestGetModified() throws Exception {
    System.out.println("*** TestGetModified ***");
    adapter.countModified("OLD", "");
    assertEquals(548, bean.getNumChangedClasses());
  }

  @AfterClass
  public static void testWriteDiffAsXMLFile() {
    // adapter.dropGraph(acronym, "OLD");
    // adapter.writeModifiedToGraph();
    // adapter.writeDiffAsXMLFile("/home/lefko/Documents/envo_mod.xml");
  }
}
