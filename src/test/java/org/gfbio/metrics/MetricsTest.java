package org.gfbio.metrics;

import static org.junit.Assert.assertEquals;
import org.gfbio.terminologies.configuration.GFBioConfiguration;
import org.gfbio.terminologies.metrics.GFBioMetricsCalculator;
import org.gfbio.terminologies.metrics.GFBioOWLMetricsCalculator;
import org.gfbio.terminologies.xml.OntologyChangesBean;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class MetricsTest {
  /*
   * ENVO 2020-06-10
   */
  private static final String acronym = "ENVO";

  private static GFBioMetricsCalculator calculator;

  private static OntologyChangesBean bean;

  @BeforeClass
  public static void setup() {
    GFBioConfiguration config = GFBioConfiguration.getInstance();
    config.setOntologyAcronym(acronym);
    config.setReleaseDate("2020-06-10");
    config.initProperties();

    bean = new OntologyChangesBean();

    calculator = new GFBioOWLMetricsCalculator(
        "file:///" + config.getRelease_path() + config.getOnto_file_name() + ".owl", bean);
  }

  @Test
  @Ignore
  public void testCalculateMetrics() {
    System.out.println("*** testCalculateMetrics ***");
    calculator.calculateMetrics();

    assertEquals(3, bean.getAvgChildren());
    assertEquals(1331, bean.getSingleChild());
    assertEquals(1065, bean.getClassesWithMore1Parent());
    assertEquals(17, bean.getMore25Children());
    assertEquals(36, bean.getMaxDepth());
    assertEquals(91, bean.getMaxChildren());
    assertEquals(3635, bean.getNumberOfLeaves());
  }



  @AfterClass
  public static void testWriteDiffAsXMLFile() {
    // adapter.dropGraph(acronym, "OLD");
    // adapter.writeModifiedToGraph();
    // adapter.writeDiffAsXMLFile("/home/lefko/Documents/envo_mod.xml");
  }
}
