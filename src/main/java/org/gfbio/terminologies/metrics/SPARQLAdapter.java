/**
 * 
 */
package org.gfbio.terminologies.metrics;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.jena.atlas.lib.NotImplemented;
import org.apache.log4j.Logger;
import org.gfbio.config.ConfigSvc;
import org.gfbio.db.VirtGraphSingleton;
import org.gfbio.terminologies.configuration.GFBioConfiguration;
import org.gfbio.terminologies.xml.ModifiedTerm;
import org.gfbio.terminologies.xml.OntologyChangesBean;
import org.gfbio.terminologies.xml.XMLRenderer;
import org.semanticweb.owlapi.model.IRI;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.RDFS;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;
import virtuoso.jena.driver.VirtuosoUpdateFactory;
import virtuoso.jena.driver.VirtuosoUpdateRequest;

/**
 *
 */
public class SPARQLAdapter extends GFBioMetricsCalculator {

  private static final Logger LOGGER = Logger.getLogger(SPARQLAdapter.class);

  private int classesCount;
  private int individualsCount;
  private int propertiesCount;
  private int classesWithoutDefinition;
  private int classesWithoutLabel;

  private String graphSuffix, GRAPHURI;

  private VirtuosoQueryExecution vqe = null;

  private OntologyChangesBean changeBean;

  private ConfigSvc configSvc = ConfigSvc.getInstance(null);

  private GFBioConfiguration config = GFBioConfiguration.getInstance();

  public SPARQLAdapter(OntologyChangesBean bean) {
    GRAPHURI = configSvc.getGraphUri();
    changeBean = bean;
  }

  @Override
  public int getClassesCount() {
    LOGGER.info("getting classes count");

    String query = new StringBuilder().append("SELECT COUNT(?cls) AS ?classesCNT")
        .append(" FROM  <" + GRAPHURI + config.getOntologyAcronym() + graphSuffix + ">")
        .append(" WHERE { ?cls rdf:type owl:Class.").append(" FILTER (!isBlank(?cls)). }")
        .toString();

    LOGGER.info(query);

    try {
      ResultSet rs = execSelect(query);

      while (rs != null && rs.hasNext()) {
        QuerySolution currentResult = rs.next();
        classesCount = currentResult.get("classesCNT").asLiteral().getInt();
      }
      vqe.close();
    } catch (Exception e) {
      LOGGER.info(e.getLocalizedMessage());
    }

    return classesCount;
  }

  @Override
  public int getIndividualsCount() {
    LOGGER.info("getting individuals count");

    String query = new StringBuilder().append("SELECT COUNT(?ind) AS ?indsCNT")
        .append(" FROM  <" + GRAPHURI + config.getOntologyAcronym() + graphSuffix + ">")
        .append(" WHERE { ?ind rdf:type owl:NamedIndividual.}").toString();

    LOGGER.info(query);

    try {
      ResultSet rs = execSelect(query);

      while (rs != null && rs.hasNext()) {
        QuerySolution currentResult = rs.next();
        individualsCount = currentResult.get("indsCNT").asLiteral().getInt();
      }
      vqe.close();
    } catch (Exception e) {
      LOGGER.info(e.getLocalizedMessage());
    }

    return individualsCount;
  }

  @Override
  public int getPropertiesCount() {
    LOGGER.info("getting properties count");

    String query = new StringBuilder().append("SELECT COUNT(?prop) AS ?propsCNT")
        .append(" FROM  <" + GRAPHURI + config.getOntologyAcronym() + graphSuffix + ">")
        .append(" WHERE { ?prop a ?p.")
        .append(" FILTER(?p = owl:DatatypeProperty || ?p = owl:ObjectProperty). }").toString();

    LOGGER.info(query);

    try {
      ResultSet rs = execSelect(query);

      while (rs != null && rs.hasNext()) {
        QuerySolution currentResult = rs.next();
        propertiesCount = currentResult.get("propsCNT").asLiteral().getInt();
      }
      vqe.close();
    } catch (Exception e) {
      LOGGER.info(e.getLocalizedMessage());
    }

    return propertiesCount;
  }

  @Override
  public int getMaxDepth() {
    throw new NotImplemented();
  }

  @Override
  public int getMaxChildren() {
    throw new NotImplemented();
  }

  @Override
  public int getAvgChildren() {
    throw new NotImplemented();
  }

  @Override
  public int getSingleChild() {
    throw new NotImplemented();
  }

  @Override
  public int getMore25Children() {
    throw new NotImplemented();
  }

  @Override
  public int getClassesWithoutDefinition() {
    LOGGER.info("getting classes without definition");

    List<String> defProps = config.getDefinition_properties();
    if (defProps != null) {
      for (String definitionProp : defProps) {

        String query = new StringBuilder().append("SELECT COUNT(DISTINCT ?c) AS ?cnt")
            .append(" FROM  <" + GRAPHURI + config.getOntologyAcronym() + graphSuffix + ">")
            .append(" WHERE { ?c rdf:type owl:Class.")
            .append(" FILTER NOT EXISTS { ?c <" + definitionProp + "> ?definition .}")
            .append(" FILTER NOT EXISTS { ?c <" + RDFS.comment + "> ?comment .}")
            .append(" FILTER (!isBlank(?c)). }").toString();

        LOGGER.info(query);

        try {
          ResultSet rs = execSelect(query);;

          while (rs != null && rs.hasNext()) {
            QuerySolution currentResult = rs.next();
            classesWithoutDefinition += currentResult.get("cnt").asLiteral().getInt();
          }
          vqe.close();
        } catch (Exception e) {
          LOGGER.info(e.getLocalizedMessage());
        }
      }
    }
    return classesWithoutDefinition;
  }

  @Override
  public int getClasesWithoutLabel() {
    LOGGER.info("getting classes without label");

    List<String> labelProps = config.getLabel_properties();

    if (labelProps != null) {

      for (String labelProp : config.getLabel_properties()) {

        String query = new StringBuilder().append("SELECT  COUNT(DISTINCT ?c) AS ?cnt")
            .append(" FROM  <" + GRAPHURI + config.getOntologyAcronym() + graphSuffix + ">")
            .append(" WHERE { ?c rdf:type owl:Class.")
            .append(" FILTER NOT EXISTS { ?c <" + labelProp + "> ?label }")
            .append(" FILTER (!isBlank(?c)). }").toString();

        LOGGER.info(query);

        try {
          ResultSet rs = execSelect(query);

          while (rs != null && rs.hasNext()) {
            QuerySolution currentResult = rs.next();
            classesWithoutLabel += currentResult.get("cnt").asLiteral().getInt();
          }
          vqe.close();
        } catch (Exception e) {
          LOGGER.info(e.getLocalizedMessage());
        }
      }
    }
    return classesWithoutLabel;
  }

  @Override
  public int getClassesWithMoreThan1Parent() {
    throw new NotImplemented();
  }

  @Override
  public int getNumberOfLeaves() {
    throw new NotImplemented();
  }

  @Override
  public String getDescriptionLogicName() {
    throw new NotImplemented();
  }

  @Override
  public void calculateMetrics() {
    changeBean.setClassesCount(this.getClassesCount());
    changeBean.setIndividualsCount(this.getIndividualsCount());
    changeBean.setPropertiesCount(this.getPropertiesCount());
    changeBean.setClassesWithoutLabel(this.getClasesWithoutLabel());
    changeBean.setClassesWithoutDefinition(this.getClassesWithoutDefinition());
  }

  /**
   * will only fire if there is an older version of the terminology available in the triple store
   */
  public void calculateDifference() throws Exception {
    if (graphExists("NEW")) {
      LOGGER.info("calculating difference metrics");
      this.countAdded("", "NEW");
      this.countDeleted("", "NEW");
      this.countModified("", "NEW");
      this.writeModifiedToGraph();
      this.writeDiffAsXMLFile(
          config.getRelease_path() + "output_diff_" + config.getOntologyAcronym() + ".xml");
    }
  }

  @Override
  public void setDefinition(String definitionProperty) {
    throw new NotImplementedException();
  }

  @Override
  public void setLabel(String labelProperty) {
    throw new NotImplementedException();
  }

  /**
   * 
   * @param old name of the graph; this assumes, that there is a graph
   *        {@link http://terminologies.gfbio.or/terms/@old@} available.
   * @return number of deleted concepts
   */
  public int countDeleted(String previous, String current) throws Exception {
    LOGGER.info("getting count of deleted terms");

    ArrayList<ModifiedTerm> deletedClasses = new ArrayList<ModifiedTerm>();

    String query = new StringBuilder().append("SELECT ?s")
        .append(" FROM <" + GRAPHURI + config.getOntologyAcronym() + current + "> ")
        .append(" FROM <" + GRAPHURI + config.getOntologyAcronym() + previous + "> ")
        .append(" WHERE { GRAPH <" + GRAPHURI + config.getOntologyAcronym() + previous
            + "> { VALUES ?type {owl:Class owl:NamedIndividual}. ?s rdf:type ?type. FILTER(!isBlank(?s)). }")
        .append(" FILTER NOT EXISTS { GRAPH <" + GRAPHURI + config.getOntologyAcronym() + current
            + "> { VALUES ?type {owl:Class owl:NamedIndividual}. ?s rdf:type ?type. FILTER(!isBlank(?s)).} } }")
        .toString();

    LOGGER.info(query);

    // try {
    ResultSet rs = execSelect(query);
    while (rs != null && rs.hasNext()) {
      QuerySolution currentResult = rs.next();
      ModifiedTerm mt = new ModifiedTerm();
      mt.setIRI(IRI.create(currentResult.get("s").asResource().getURI()));
      deletedClasses.add(mt);
    }
    vqe.close();
    // } catch (Exception e) {
    // e.printStackTrace();
    // }

    changeBean.setNumDeletedClasses(deletedClasses.size());
    changeBean.setDeletedClasses(deletedClasses);

    return deletedClasses.size();
  }


  /**
   * 
   * @param latest name of the graph; this assumes, that there is a graph
   *        {@link http://terminologies.gfbio.or/terms/@latest@} available.
   * @return number of added concepts
   */
  public int countAdded(String previous, String current) throws Exception {
    LOGGER.info("getting count of added terms");

    ArrayList<ModifiedTerm> newClasses = new ArrayList<ModifiedTerm>();

    String query = new StringBuilder().append("SELECT ?s")
        .append(" FROM <" + GRAPHURI + config.getOntologyAcronym() + current + "> ")
        .append(" FROM <" + GRAPHURI + config.getOntologyAcronym() + previous + "> ")
        .append(" WHERE { GRAPH <" + GRAPHURI + config.getOntologyAcronym() + current
            + "> { VALUES ?type {owl:Class owl:NamedIndividual}. ?s rdf:type ?type. FILTER(!isBlank(?s)). }")
        .append(" FILTER NOT EXISTS { GRAPH <" + GRAPHURI + config.getOntologyAcronym() + previous
            + "> { VALUES ?type {owl:Class owl:NamedIndividual}. ?s rdf:type ?type. FILTER(!isBlank(?s)).} } }")
        .toString();

    LOGGER.info(query);

    ResultSet rs = execSelect(query);
    while (rs != null && rs.hasNext()) {
      QuerySolution currentResult = rs.next();
      ModifiedTerm mt = new ModifiedTerm();
      mt.setIRI(IRI.create(currentResult.get("s").asResource().getURI()));
      newClasses.add(mt);
    }
    vqe.close();

    changeBean.setNumNewClasses(newClasses.size());
    changeBean.setNewClasses(newClasses);

    return newClasses.size();
  }

  /**
   * 
   * @param previous
   * @param current
   */
  public int countModified(String previous, String current) throws Exception {
    LOGGER.info("getting count of modified terms");

    Instant start = Instant.now();

    ArrayList<ModifiedTerm> modifiedTerms = new ArrayList<ModifiedTerm>();

    for (String labelProp : config.getLabel_properties()) {

      // (1) find all terms, which have changed from previous to current version (independently of
      // the
      // change itself)
      String query = new StringBuilder()
          .append(
              "SELECT ?s ?p str(?o) AS ?olabel str(?sl) AS ?slabel str(?label) AS ?superClassLabel")
          .append(" FROM  <" + GRAPHURI + config.getOntologyAcronym() + previous + ">")
          .append(" FROM  <" + GRAPHURI + config.getOntologyAcronym() + current + ">")
          .append(" WHERE { {").append(" SELECT * WHERE {")
          .append(" GRAPH <" + GRAPHURI + config.getOntologyAcronym() + previous + "> {")
          .append(
              " ?s ?p ?o . ?s <" + labelProp + "> ?sl . OPTIONAL {?o <" + labelProp + "> ?label.}")
          .append(" FILTER(!isBlank(?s)) . FILTER(!isBlank(?o)) . }").append(" FILTER NOT EXISTS {")
          .append(" GRAPH <" + GRAPHURI + config.getOntologyAcronym() + current + "> {")
          .append(" ?s ?p ?o .").append(" FILTER(!isBlank(?s)) . } }").append(" FILTER EXISTS {")
          .append(" GRAPH <" + GRAPHURI + config.getOntologyAcronym() + current + "> {")
          .append(" VALUES ?type {owl:Class owl:NamedIndividual}. ?s rdf:type ?type.")
          .append(" FILTER(!isBlank(?s)) . } }")
          .append(" FILTER(?p = rdfs:subClassOf || ?p = <" + labelProp + "> || "
              + config.propertiesAsParameter(config.getSynonym_properties())
              + ") . } } } ORDER BY ?s")
          .toString();

      LOGGER.info(query);
      String changedURI = "";
      ModifiedTerm mt = null;

      // try {
      ResultSet rs = execSelect(query);
      while (rs != null && rs.hasNext()) {
        QuerySolution currentResult = rs.next();
        Resource subject = currentResult.get("s").asResource();
        Resource prop = currentResult.get("p").asResource();

        // in case there are multiple changes for the same URI
        if (!changedURI.equalsIgnoreCase(subject.toString())) {
          if (mt != null)
            modifiedTerms.add(mt);
          changedURI = subject.toString();
          mt = new ModifiedTerm();
          mt.setIRI(IRI.create(changedURI));
          mt.setTermLabel(currentResult.get("slabel").toString());
        }

        // check for changed labels
        if (prop.toString().equalsIgnoreCase(labelProp)) {
          String delAnnotation = "Label '" + currentResult.get("olabel") + "'";
          mt.addDeletedAnnotation(delAnnotation);

          // get the change from the current graph
          mt.addAddedAnnotation(
              "Label '" + getModifiedLabel(mt.getIRI().toString(), current, labelProp) + "'");
        }

        // check for changed synonyms
        // prop = currentResult.get("p").asResource();
        if (config.propertiesAsParameter(config.getSynonym_properties())
            .contains(prop.toString())) {
          String delAnnotation = prop.getLocalName() + " '" + currentResult.get("olabel") + "'";
          mt.addDeletedAnnotation(delAnnotation);

          // get the change from the current graph
          for (String synonym : getModifiedSynonym(mt.getIRI().toString(), previous, current))
            mt.addAddedAnnotation(synonym);
        }

        // check for changed axioms, e.g., subClassOf
        // [http://purl.obolibrary.org/obo/PATO_0000051,
        // http://www.w3.org/2000/01/rdf-schema#subClassOf,
        // http://purl.obolibrary.org/obo/BFO_0000019, "quality"@en]
        // prop = currentResult.get("p").asResource().toString();
        if (prop.toString().equalsIgnoreCase(RDFS.subClassOf.getURI())) {
          mt.addDeletedAxiom("'" + currentResult.get("slabel") + "' SubClassOf '"
              + currentResult.get("superClassLabel") + "'");

          // get the change from the current graph
          for (String label : getModifiedSubClasses(mt.getIRI().toString(), previous, current))
            mt.addAddedAxiom("'" + currentResult.get("slabel") + "' SubClassOf '" + label + "'");
        }
        if (rs.hasNext() == false)
          modifiedTerms.add(mt);
      }
      vqe.close();
      // } catch (Exception e) {
      // e.printStackTrace();
      // }
    }
    changeBean.setNumChangedClasses(modifiedTerms.size());
    changeBean.setClassesWithDifferences(modifiedTerms);

    Instant end = Instant.now();
    LOGGER.info("Processing took " + configSvc.formatDuration(Duration.between(start, end)));

    return modifiedTerms.size();
  }

  private List<String> getModifiedSubClasses(String URI, String previous, String current) {
    ArrayList<String> subClasses = new ArrayList<String>();

    for (String labelProp : config.getLabel_properties()) {

      String query = new StringBuilder().append("SELECT str(?label) AS ?slabel")
          .append(" FROM <" + GRAPHURI + config.getOntologyAcronym() + previous + ">")
          .append(" FROM <" + GRAPHURI + config.getOntologyAcronym() + current + ">")
          .append(" WHERE { GRAPH <" + GRAPHURI + config.getOntologyAcronym() + current + "> { ")
          .append(" <" + URI + "> ?p ?o . OPTIONAL {?o <" + labelProp + "> ?label.}")
          .append(" FILTER(!isBlank(?o)) . }").append(" FILTER NOT EXISTS { ")
          .append(" GRAPH <" + GRAPHURI + config.getOntologyAcronym() + previous + "> {")
          .append(" <" + URI + "> ?p ?o . } }").append(" FILTER EXISTS {")
          .append(" GRAPH <" + GRAPHURI + config.getOntologyAcronym() + previous + "> { <" + URI
              + "> rdf:type owl:Class . } } ")
          .append(" FILTER(?p = <" + RDFS.subClassOf + ">) . }").toString();

      // LOGGER.info(query);

      try {
        ResultSet rs = execSelect(query);
        while (rs != null && rs.hasNext()) {
          QuerySolution currentResult = rs.next();
          if (currentResult.get("slabel") != null)
            subClasses.add(currentResult.get("slabel").asLiteral().toString());
        }
        vqe.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    return subClasses;
  }

  private List<String> getModifiedSynonym(String URI, String previous, String current) {

    ArrayList<String> synonyms = new ArrayList<String>();

    String query = new StringBuilder().append("SELECT ?p str(?o) AS ?label")
        .append(" FROM <" + GRAPHURI + config.getOntologyAcronym() + previous + ">")
        .append(" FROM <" + GRAPHURI + config.getOntologyAcronym() + current + ">")
        .append(" WHERE { GRAPH <" + GRAPHURI + config.getOntologyAcronym() + current + "> { ")
        .append(" <" + URI + "> ?p ?o .").append(" FILTER(!isBlank(?o)) . }")
        .append(" FILTER NOT EXISTS { ")
        .append(" GRAPH <" + GRAPHURI + config.getOntologyAcronym() + previous + "> {")
        .append(" <" + URI + "> ?p ?o . } }").append(" FILTER EXISTS {")
        .append(" GRAPH <" + GRAPHURI + config.getOntologyAcronym() + previous + "> { <" + URI
            + "> rdf:type owl:Class . } } ")
        .append(" FILTER(" + config.propertiesAsParameter(config.getSynonym_properties()) + ") . }")
        .toString();

    // LOGGER.info(query);

    try {
      ResultSet rs = execSelect(query);
      while (rs != null && rs.hasNext()) {
        QuerySolution currentResult = rs.next();
        if (currentResult.get("p") != null)
          synonyms.add(currentResult.get("p").asResource().getLocalName() + " '"
              + currentResult.get("label").asLiteral().toString() + "'");
      }
      vqe.close();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return synonyms;
  }

  private String getModifiedLabel(String URI, String current, String labelProp) {

    String query = new StringBuilder().append("SELECT str(?label) AS ?slabel")
        .append(" FROM <" + GRAPHURI + config.getOntologyAcronym() + current + ">")
        .append(" WHERE { <" + URI + "> ?p ?label . ").append(" FILTER(?p = <" + labelProp + ">) ")
        .append(" } ").toString();

    // LOGGER.info(query);

    try {
      ResultSet rs = execSelect(query);
      while (rs != null && rs.hasNext()) {
        QuerySolution currentResult = rs.next();
        if (currentResult.get("slabel") != null)
          return currentResult.get("slabel").asLiteral().toString();
      }
      vqe.close();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return "N.A.";
  }

  /**
   * If an empty string is supplied, it searches for the default graph of the set terminolgy, e.g.,
   * <http://terminologies.gfbio.org/terms/ENVO>
   * 
   * @param nameSuffix For querying a (temporal) graph, e.g., ENVOTEMP
   * @return
   */
  public boolean graphExists(String nameSuffix) {
    boolean exists = false;

    String query = new StringBuilder().append("SELECT COUNT(*) as ?cnt")
        .append(" FROM <" + GRAPHURI + config.getOntologyAcronym() + nameSuffix + ">")
        .append(" WHERE { ?s ?p ?o } ").toString();

    LOGGER.info(query);

    ResultSet rs = execSelect(query);
    if (rs != null && rs.hasNext()) {
      QuerySolution count = rs.next();
      // LOGGER.info(count.get("cnt").asLiteral().getInt());
      exists = count.get("cnt").asLiteral().getInt() == 0 ? false : true;
    }

    vqe.close();
    // LOGGER.info(exists);
    return exists;
  }

  public ResultSet execSelect(String query) {
    ResultSet rs = null;

    try {

      VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
      vqe = VirtuosoQueryExecutionFactory.create(query, set);
      vqe.setTimeout(1);
      rs = vqe.execSelect();
      // DO NOT CLOSE vqe just yet!

    } catch (Exception e) {
      e.printStackTrace();
    }

    return rs;
  }

  /**
   * Creates a copy of the graph, which is called, e.g.,
   * {@code  <http://terminologies.gfbio.org/terms/ENVOOLD>}
   * 
   * @param acronym
   * @return
   */
  public void backupGraph(String acronym) {

    if (graphExists("")) {
      String query = "COPY <" + GRAPHURI + acronym + "> TO <" + GRAPHURI + acronym + "OLD>";
      LOGGER.info(query);
      execSelect(query);
      vqe.close();
    }
  }

  public void dropGraph(String acronym, String suffix) {
    String query = "DROP SILENT GRAPH <" + GRAPHURI + acronym + suffix + ">";
    LOGGER.info(query);
    execSelect(query);
    vqe.close();
  }

  public void renameGraph(String acronym, String suffix, String renameTo) {
    if (graphExists(suffix)) {
      String query =
          "COPY <" + GRAPHURI + acronym + suffix + "> TO <" + GRAPHURI + acronym + renameTo + ">";
      LOGGER.info(query);
      execSelect(query);
      vqe.close();
    }
  }

  public void writeModifiedToGraph() {

    // clear any existing modified graph first
    if (graphExists("MOD"))
      dropGraph(config.getOntologyAcronym(), "MOD");

    VirtuosoUpdateRequest vur = null;
    String query = null;

    try {

      // create initial empty model
      Model model = ModelFactory.createDefaultModel();

      LOGGER.info("writing modified terms");
      for (ModifiedTerm mt : changeBean.getClassesWithDifferences()) {

        Statement statement = model.createStatement(
            model.createResource(GRAPHURI + config.getOntologyAcronym().hashCode()),
            model.createProperty(GRAPHURI + "ontology#modified_term"),
            model.createResource(mt.getIRI().toString()));

        model.add(statement);

        query = "INSERT INTO GRAPH <" + GRAPHURI + config.getOntologyAcronym() + "MOD> { <"
            + model.createResource(GRAPHURI + config.getOntologyAcronym().hashCode()) + "> <"
            + model.createProperty(GRAPHURI + "ontology#modified_term") + "> <"
            + model.createResource(mt.getIRI().toString()) + "> .}";

        vur = VirtuosoUpdateFactory.create(query, VirtGraphSingleton.getInstance().getVirtGraph());
        vur.exec();

        statement = model.createStatement(model.createResource(mt.getIRI().toString()),
            model.createProperty(GRAPHURI + "ontology#change"), model.createLiteral("modified"));

        model.add(statement);

        query = "INSERT INTO GRAPH <" + GRAPHURI + config.getOntologyAcronym() + "MOD> { <"
            + model.createResource(mt.getIRI().toString()) + "> <"
            + model.createProperty(GRAPHURI + "ontology#change") + "> '"
            + model.createLiteral("modified") + "' .}";

        vur = VirtuosoUpdateFactory.create(query, VirtGraphSingleton.getInstance().getVirtGraph());
        vur.exec();
      }

      LOGGER.info("writing added terms");
      for (ModifiedTerm mt : changeBean.getNewClasses()) {

        Statement statement = model.createStatement(
            model.createResource(GRAPHURI + config.getOntologyAcronym().hashCode()),
            model.createProperty(GRAPHURI + "ontology#modified_term"),
            model.createResource(mt.getIRI().toString()));

        model.add(statement);

        query = "INSERT INTO GRAPH <" + GRAPHURI + config.getOntologyAcronym() + "MOD> { <"
            + model.createResource(GRAPHURI + config.getOntologyAcronym().hashCode()) + "> <"
            + model.createProperty(GRAPHURI + "ontology#modified_term") + "> <"
            + model.createResource(mt.getIRI().toString()) + "> .}";

        vur = VirtuosoUpdateFactory.create(query, VirtGraphSingleton.getInstance().getVirtGraph());
        vur.exec();

        statement = model.createStatement(model.createResource(mt.getIRI().toString()),
            model.createProperty(GRAPHURI + "ontology#change"), model.createLiteral("added"));

        model.add(statement);

        query = "INSERT INTO GRAPH <" + GRAPHURI + config.getOntologyAcronym() + "MOD> { <"
            + model.createResource(mt.getIRI().toString()) + "> <"
            + model.createProperty(GRAPHURI + "ontology#change") + "> '"
            + model.createLiteral("added") + "' .}";

        vur = VirtuosoUpdateFactory.create(query, VirtGraphSingleton.getInstance().getVirtGraph());
        vur.exec();
      }

      LOGGER.info("writing removed terms");
      for (ModifiedTerm mt : changeBean.getDeletedClasses()) {

        Statement statement = model.createStatement(
            model.createResource(GRAPHURI + config.getOntologyAcronym().hashCode()),
            model.createProperty(GRAPHURI + "ontology#modified_term"),
            model.createResource(mt.getIRI().toString()));

        model.add(statement);

        query = "INSERT INTO GRAPH <" + GRAPHURI + config.getOntologyAcronym() + "MOD> { <"
            + model.createResource(GRAPHURI + config.getOntologyAcronym().hashCode()) + "> <"
            + model.createProperty(GRAPHURI + "ontology#modified_term") + "> <"
            + model.createResource(mt.getIRI().toString()) + "> .}";

        vur = VirtuosoUpdateFactory.create(query, VirtGraphSingleton.getInstance().getVirtGraph());
        vur.exec();

        statement = model.createStatement(model.createResource(mt.getIRI().toString()),
            model.createProperty(GRAPHURI + "ontology#change"), model.createLiteral("removed"));

        model.add(statement);

        query = "INSERT INTO GRAPH <" + GRAPHURI + config.getOntologyAcronym() + "MOD> { <"
            + model.createResource(mt.getIRI().toString()) + "> <"
            + model.createProperty(GRAPHURI + "ontology#change") + "> '"
            + model.createLiteral("removed") + "' .}";

        vur = VirtuosoUpdateFactory.create(query, VirtGraphSingleton.getInstance().getVirtGraph());
        vur.exec();
      }

      // all done, now write triples to a file
      // model.write(
      // new FileOutputStream(
      // configSvc.getWorkDir() + "/modified_terms_" + config.getOntologyAcronym() + ".ttl"),
      // "N-TRIPLE");
      model.write(
          new FileOutputStream(
              config.getRelease_path() + "modified_terms_" + config.getOntologyAcronym() + ".ttl"),
          "N-TRIPLE");

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  /**
   * write diffs as an XML file which includes a link to the xsl transform file for rendering the
   * xml if no value or null is entered for pathToXSLT parameter then default xslt file name is
   * written
   *
   * @param filePath Location of file to write results to
   */
  public void writeDiffAsXMLFile(String filePath) {


    try {
      // iterate through list, writing to file
      XMLRenderer xmlRenderer = new XMLRenderer();
      xmlRenderer.writeDiffAsXMLFile(filePath, this.changeBean);

    } catch (IOException e) {
      LOGGER.info(
          "An error occurred when attempt to write the diff as XML to a file at: " + filePath);
      e.printStackTrace();
    }
    convertXML2JSON(filePath);
  }

  private int convertXML2JSON(String xmlFilePath) {

    int exitCode = 0;

    try {

      LOGGER.info("converting XML to JSON");

      List<String> command = new ArrayList<String>();
      command.add("python3");
      command.add(configSvc.getWorkDir() + "XML2JSON.py");
      command.add(xmlFilePath);
      command.add(Paths.get(xmlFilePath).getParent() + "/" + "output_diff_"
          + config.getOntologyAcronym() + ".json");

      LOGGER.info("parsing command " + command);

      ProcessBuilder pb = new ProcessBuilder(command);
      pb.inheritIO();
      Process process;

      process = pb.start();

      exitCode = process.waitFor();
      if (exitCode == 0)
        LOGGER.info("process completed");

    } catch (IOException | InterruptedException e) {
      LOGGER.error("Conversion failed due to " + e.getLocalizedMessage());
      if (e.getLocalizedMessage().contains("error=2")) {
        try {
          // presumably python3 is not installed, but python(2) should be
          // Cannot run program "python3": error=2, Datei oder Verzeichnis nicht gefunden
          LOGGER.info("running with python(2) again");
          List<String> command = new ArrayList<String>();
          command.add("python");
          command.add(configSvc.getWorkDir() + "XML2JSON.py");
          command.add(xmlFilePath);
          command.add(Paths.get(xmlFilePath).getParent() + "/" + "output_diff_"
              + config.getOntologyAcronym() + ".json");

          LOGGER.info("parsing command " + command);

          ProcessBuilder pb = new ProcessBuilder(command);
          pb.inheritIO();
          Process process;

          process = pb.start();

          exitCode = process.waitFor();
          if (exitCode == 0)
            LOGGER.info("process completed");
        } catch (IOException | InterruptedException e2) {
          // now it must be something else
          LOGGER.error("Conversion failed due to " + e2.getLocalizedMessage());
        }
      }
    }

    return exitCode;
  }

  @Override
  public void setDescriptionLogicName(String descriptionLogicName) {
    throw new NotImplementedException();

  }

  @Override
  public void setAcronym(String acronym) {
    throw new NotImplementedException();
  }

  public void setGraphSuffix(String graphSuffix) {
    this.graphSuffix = graphSuffix;
  }
}
