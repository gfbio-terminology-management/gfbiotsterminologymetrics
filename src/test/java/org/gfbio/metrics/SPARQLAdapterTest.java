/**
 * 
 */
package org.gfbio.metrics;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import org.gfbio.terminologies.metrics.SPARQLAdapter;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * This tests requires a running Virtuoso instance, preferably @ localhost:1111
 */
public class SPARQLAdapterTest {

  // FLOPO version 2019-09-11
  private static final String acronym = "ENVO";

  private static SPARQLAdapter adapter;

  @BeforeClass
  public static void setup() {
    adapter = new SPARQLAdapter();
    adapter.setAcronym(acronym);
    adapter.setOntologyUrl("/var/opt/ts/ontologies/envo/2020-06-10/envo.owl");
    adapter.setDefinition("obo:IAO_0000115");
    adapter.setLabel("rdfs:label");
    List<String> synonymProperties = new ArrayList<String>();
    synonymProperties.add("oboInOwl:hasExactSynonym");
    synonymProperties.add("oboInOwl:hasRelatedSynonym");
    synonymProperties.add("oboInOwl:hasBroaderSynonym");
    adapter.setSynonyms(synonymProperties);
  }

  @Test
  @Ignore
  public void testGetClassesCount() {
    System.out.println("** testGetClassesCount **");

    assertEquals(6199, adapter.getClassesCount());
  }

  @Test
  @Ignore
  public void testGetIndividualsCount() {
    System.out.println("** testGetIndividualsCount **");

    assertEquals(44, adapter.getIndividualsCount());
  }

  @Test
  @Ignore
  public void testGetPropertiesCount() {
    System.out.println("** testGetPropertiesCount **");

    assertEquals(135, adapter.getPropertiesCount());
  }

  @Test
  @Ignore
  public void testGetClasesWithoutLabel() {
    System.out.println("** testGetClasesWithoutLabel **");

    assertEquals(0, adapter.getClasesWithoutLabel());
  }

  @Test
  @Ignore
  public void testGetClassesWithoutDefinition() {
    System.out.println("** testGetClassesWithoutDefinition **");

    assertEquals(1977, adapter.getClassesWithoutDefinition());
  }

  @AfterClass
  public static void testWriteDiffAsXMLFile() {
    adapter.writeDiffAsXMLFile("/home/lefko/gfbio/myDiff.xml");
  }

  @Test
  public void testCountDeleted() {
    System.out.println("** testCountDeleted **");

    assertEquals(2364, adapter.countDeleted("", "2020"));
  }

  @Test
  public void testCountAdded() {
    System.out.println("** testCountAdded **");

    assertEquals(1654, adapter.countAdded("", "2020"));
  }

  @Test
  public void testGetModifiedTerms() {
    System.out.println("** testGetModifiedTerms **");
    adapter.getModifiedTerms("", "2020");
  }

}
