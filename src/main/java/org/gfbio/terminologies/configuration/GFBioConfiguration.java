package org.gfbio.terminologies.configuration;

import java.util.List;
import java.util.stream.Collectors;
import org.gfbio.config.ConfigSvc;
import com.hp.hpl.jena.shared.NotFoundException;

/**
 * This acts as a container for any ontology-related properties, which are needed for the metrics
 * calculation. Properties will be populated by instantiating the {@link org.gfbio.config.ConfigSvc}
 * and accessing the triple-store's parameter graph.
 *
 */
public class GFBioConfiguration {

  private static GFBioConfiguration instance;

  // path to ontologies
  private String ontologies_path;

  // path to specific ontology folder in ontologies_path, e.g., /var/opt/ts/ontologies/ENVO/
  private String release_path;

  // must not be the same as the acronym
  private String onto_file_name;

  private String ontology_acronym;
  private String ontology_name;
  private String ontology_description;
  private String ontology_iri;
  private String release_date;
  private List<String> label_properties;
  private List<String> synonym_properties;
  private List<String> definition_properties;
  private boolean calculate_complex_metrics;

  private ConfigSvc configSvc;

  public static GFBioConfiguration getInstance() {
    if (GFBioConfiguration.instance == null) {
      GFBioConfiguration.instance = new GFBioConfiguration();
    }
    return GFBioConfiguration.instance;
  }


  public GFBioConfiguration() {
    // from gfbioapi project
    configSvc = ConfigSvc.getInstance(null);
  }

  /**
   * read initial properties from ConfigSvc
   */
  public void initProperties() {

    ontologies_path = configSvc.getWorkDir() + "ontologies/";

    try {
      onto_file_name = configSvc.getParameterFromGraph(this.ontology_acronym, "filename").get(0);
      calculate_complex_metrics = Boolean.valueOf(
          configSvc.getParameterFromGraph(this.ontology_acronym, "supportsComplexMetrics").get(0));

      List<String> props = configSvc.getParameterFromGraph(ontology_acronym, "label");
      if (props != null)
        label_properties = props.stream().map(label -> configSvc.resolveNamespaceToUri(label))
            .collect(Collectors.toList());

      props = configSvc.getParameterFromGraph(ontology_acronym, "synonym");
      if (props != null)
        synonym_properties = props.stream().map(synonym -> configSvc.resolveNamespaceToUri(synonym))
            .collect(Collectors.toList());

      props = configSvc.getParameterFromGraph(ontology_acronym, "definition");
      if (props != null)
        definition_properties =
            props.stream().map(definition -> configSvc.resolveNamespaceToUri(definition))
                .collect(Collectors.toList());
    } catch (NotFoundException e) {
      e.printStackTrace();
      System.exit(-1);
    }

    this.release_path = ontologies_path + this.ontology_acronym + "/" + this.release_date + "/";
  }

  public String propertiesAsParameter(List<String> props) {
    if (props == null)
      return "<N.A.>";
    StringBuilder sb = new StringBuilder();
    for (String s : props) {
      sb.append(" ?p = <" + configSvc.resolveNamespaceToUri(s) + "> ||");
    }
    int len = sb.length() - 2;
    sb.delete(len, len + 2);
    return sb.toString();
  }

  /**
   * Path to ontologies, e.g., /var/opt/ts/ontologies/
   * 
   * @return
   */
  public String getOntologiesPath() {
    return ontologies_path;
  }

  /**
   * 
   * @return Written as yyyy-mm-dd
   */
  public String getReleaseDate() {
    return release_date;
  }

  /**
   * 
   * @param date Written as yyyy-mm-dd
   */
  public void setReleaseDate(String date) {
    this.release_date = date;
  }

  /**
   * 
   * @return Uppercase ontology name, e.g., ENVO
   */
  public String getOntologyAcronym() {
    return ontology_acronym;
  }

  /**
   * 
   * @param acronym Uppercase ontology name, e.g., ENVO
   */
  public void setOntologyAcronym(String acronym) {
    this.ontology_acronym = acronym;
  }

  public String getOntologyName() {
    return ontology_name;
  }

  public String getOntologyDescription() {
    return ontology_description;
  }

  public String getOntologyIRI() {
    return ontology_iri;
  }

  public boolean getComplexMetrics() {
    return calculate_complex_metrics;
  }

  /**
   * 
   * @return path to specific ontology folder in ontologies_path, e.g., /var/opt/ts/ontologies/ENVO/
   */
  public String getRelease_path() {
    return release_path;
  }

  public String getOnto_file_name() {
    return onto_file_name;
  }

  public List<String> getLabel_properties() {
    return label_properties;
  }

  public List<String> getSynonym_properties() {
    return synonym_properties;
  }

  public List<String> getDefinition_properties() {
    return definition_properties;
  }
}
