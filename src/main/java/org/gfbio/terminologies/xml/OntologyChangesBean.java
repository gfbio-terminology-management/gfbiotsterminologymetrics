package org.gfbio.terminologies.xml;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.text.StringEscapeUtils;
import org.semanticweb.owlapi.model.IRI;


public class OntologyChangesBean {

  private String previousVersion, currentVersion;

  /**
   * Metric Properties (minimal)
   */
  private int classesWithoutDefinition;
  private int classesWithoutLabel;
  private int classesCount;
  private int individualsCount;
  private int propertiesCount;

  /**
   * Metric Properties (complex)
   */
  private int maxDepth;
  private int maxChildren;
  private int avgChildren;
  private int singleChild;
  private int more25Children;
  private int classesWithMore1Parent;
  private int numberOfLeaves;
  private String descriptionLogicName;


  /**
   * Difference Properties
   */
  private ArrayList<ModifiedTerm> classesWithDifferences = new ArrayList<ModifiedTerm>();
  private ArrayList<ModifiedTerm> newClasses = new ArrayList<ModifiedTerm>();
  private ArrayList<ModifiedTerm> deletedClasses = new ArrayList<ModifiedTerm>();
  private ArrayList<String> classesWithDifferencesAsXML = new ArrayList<String>();
  private ArrayList<String> newClassesAsXML = new ArrayList<String>();
  private ArrayList<String> deletedClassesAsXML = new ArrayList<String>();
  private int numChangedClasses;
  private int numNewClasses;
  private int numDeletedClasses;
  private Exception exceptionDuringDiff;
  private String errorCause;


  public int getNumChangedClasses() {
    return this.numChangedClasses;
  }

  public int getNumNewClasses() {
    return this.numNewClasses;
  }

  public int getNumDeletedClasses() {
    return this.numDeletedClasses;
  }

  public ArrayList<String> getClassesWithDifferencesAsXML() {
    return this.classesWithDifferencesAsXML;
  }

  public ArrayList<String> getNewClassesAsXML() {
    return this.newClassesAsXML;
  }

  public ArrayList<String> getDeletedClassesAsXML() {
    return this.deletedClassesAsXML;
  }

  /**
   * set the classes with differences variable
   * 
   * @param classesWithDifferences the classesWithDifferences to set
   */
  public void setClassesWithDifferences(ArrayList<ModifiedTerm> classesWithDifferences) {
    this.classesWithDifferences = classesWithDifferences;
    this.createClassChangesAsXML();
  }



  /**
   * method for formatting the class changes in xml for output to browser
   */
  public void createClassChangesAsXML() {

    // add open xml tag for this type of change
    this.classesWithDifferencesAsXML.add("<changedClasses>");

    // create the classes with difference as strings which is useful for displaying purposes
    // first loop through each OWLClassAxiomsInfo object which represents a single class and its
    // changes
    for (int i = 0; i < this.classesWithDifferences.size(); i++) {
      ModifiedTerm singleChangedClass = this.classesWithDifferences.get(i);

      // add open xml tag for this change
      this.classesWithDifferencesAsXML.add("<changedClass>");

      // first get the class URI
      IRI classIRI = singleChangedClass.getIRI();

      // add class URI
      String changesIRIXML = ("<classIRI>" + classIRI.toString() + "</classIRI>");
      // add to list
      this.classesWithDifferencesAsXML.add(changesIRIXML);

      String labelXML =
          ("<classLabel>" + singleChangedClass.getTermLabel().replace("<", "(").replace(">", ")")
              + "</classLabel>");
      // add to list
      this.classesWithDifferencesAsXML.add(labelXML);

      // add deleted annotations
      List<String> deletedAnnotations = singleChangedClass.getDeletedAnnotations();
      if (deletedAnnotations != null) {
        Iterator<String> it = deletedAnnotations.iterator();
        while (it.hasNext()) {
          String label = it.next();
          // strip out any chevrons that owl-api spits out
          // when there is only a uri as this breaks the xml
          label = label.replaceAll("<", "");
          label = label.replaceAll(">", "");

          // check the label for any reserved xml characters and replace them with correct encoding
          String finalLabel = checkForReservedXMLCharacter(label);

          String annotationXML = ("<deletedAnnotation>" + finalLabel + "</deletedAnnotation>");
          this.classesWithDifferencesAsXML.add(annotationXML);
        }
      }

      // add new annotations
      List<String> newAnnotations = singleChangedClass.getAddedAnnotations();
      if (newAnnotations != null) {
        Iterator<String> it = newAnnotations.iterator();
        while (it.hasNext()) {
          String label = it.next();
          // strip out any chevrons that owl-api spits out
          // when there is only a uri as this breaks the xml
          label = label.replaceAll("<", "");
          label = label.replaceAll(">", "");

          // check the label for any reserved xml characters and replace them with correct encoding
          String finalLabel = checkForReservedXMLCharacter(label);

          String annotationXML = ("<newAnnotation>" + finalLabel + "</newAnnotation>");
          this.classesWithDifferencesAsXML.add(annotationXML);
        }
      }

      // add deleted axioms
      List<String> deletedAxioms = singleChangedClass.getDeletedAxioms();
      if (deletedAxioms != null) {
        Iterator<String> it = deletedAxioms.iterator();
        while (it.hasNext()) {
          String label = it.next();
          // strip out any chevrons that owl-api spits out
          // when there is only a uri as this breaks the xml
          label = label.replaceAll("<", "");
          label = label.replaceAll(">", "");

          // check the label for any reserved xml characters and replace them with correct encoding
          String finalLabel = checkForReservedXMLCharacter(label);

          String axiomXML = ("<deletedAxiom>" + finalLabel + "</deletedAxiom>");
          this.classesWithDifferencesAsXML.add(axiomXML);
        }
      }


      // add new axioms
      List<String> newAxioms = singleChangedClass.getAddedAxioms();
      if (newAxioms != null) {
        Iterator<String> it = newAxioms.iterator();
        while (it.hasNext()) {
          String label = it.next();
          // strip out any chevrons that owl-api spits out
          // when there is only a uri as this breaks the xml
          label = label.replaceAll("<", "");
          label = label.replaceAll(">", "");

          // check the label for any reserved xml characters and replace them with correct encoding
          String finalLabel = checkForReservedXMLCharacter(label);

          String axiomXML = ("<newAxiom>" + finalLabel + "</newAxiom>");
          this.classesWithDifferencesAsXML.add(axiomXML);
        }
      }
      // close xml tag for this change
      this.classesWithDifferencesAsXML.add("</changedClass>");
    } // end for loop going through each class

    // add open xml tag for this type of change
    this.classesWithDifferencesAsXML.add("</changedClasses>");
  }

  /**
   * check for reserved XML characters
   *
   * @param unformattedXml
   * @return formattedXml formatted for xml
   */
  private String checkForReservedXMLCharacter(String unformattedXml) {
    String formattedXml = StringEscapeUtils.escapeXml11(unformattedXml);

    return formattedXml;
  }


  /**
   * method to set the new classes added between two ontology
   * 
   * @param newClasses the new classes to set
   */
  public void setNewClasses(ArrayList<ModifiedTerm> newClasses) {
    this.newClasses = newClasses;
    this.createNewClassesAsXML();
  }


  /**
   * method for formatting the new classes in xml for output to browser
   */
  public void createNewClassesAsXML() {

    // add open xml tag for this type of change
    this.newClassesAsXML.add("<newClasses>");

    // create the new classes as strings which is useful for displaying purposes
    // first loop through each OWLClassAxiomsInfo object which represents a single class and its
    // changes
    for (int i = 0; i < this.newClasses.size(); i++) {
      ModifiedTerm singleClass = this.newClasses.get(i);

      // add open xml tag for this change
      this.newClassesAsXML.add("<newClass>");

      // first get the class URI
      IRI classIRI = singleClass.getIRI();

      // add class IRI
      String changesIRIXML = ("<classIRI>" + classIRI.toString() + "</classIRI>");
      // add to list
      this.newClassesAsXML.add(changesIRIXML);

      // close xml tag for this new class
      this.newClassesAsXML.add("</newClass>");
    } // end for loop going through each class

    // add closing xml tag for this type of change
    this.newClassesAsXML.add("</newClasses>");
  }


  /**
   * method to set the new classes added between two ontology
   * 
   * @param deletedClasses the deleted classes to set in the bean
   */
  public void setDeletedClasses(ArrayList<ModifiedTerm> deletedClasses) {
    this.deletedClasses = deletedClasses;
    this.createDeletedClassesAsXML();
  }


  /**
   * method for formatting the deleted classes in xml for output to browser
   */
  public void createDeletedClassesAsXML() {

    // add open xml tag for this type of change
    this.deletedClassesAsXML.add("<deletedClasses>");

    // create the new classes as strings which is useful for displaying purposes
    // first loop through each OWLClassAxiomsInfo object which represents a single class and its
    // changes
    for (int i = 0; i < this.deletedClasses.size(); i++) {
      ModifiedTerm singleClass = this.deletedClasses.get(i);

      // add open xml tag for this change
      this.deletedClassesAsXML.add("<deletedClass>");

      // first get the class URI
      IRI classIRI = singleClass.getIRI();

      // add class IRI
      String changesIRIXML = ("<classIRI>" + classIRI.toString() + "</classIRI>");
      // add to list
      this.deletedClassesAsXML.add(changesIRIXML);

      // close xml tag for this change
      this.deletedClassesAsXML.add("</deletedClass>");

    } // end for loop going through each class
      // add open xml tag for this type of change
    this.deletedClassesAsXML.add("</deletedClasses>");
  }

  /**
   * @return the classesWithDifferences
   */
  public ArrayList<ModifiedTerm> getClassesWithDifferences() {
    return classesWithDifferences;
  }

  /**
   * @return the newClasses
   */
  public ArrayList<ModifiedTerm> getNewClasses() {
    return newClasses;
  }

  /**
   * @return the deletedClasses
   */
  public ArrayList<ModifiedTerm> getDeletedClasses() {
    return deletedClasses;
  }


  public void setNumChangedClasses(int i) {
    this.numChangedClasses = i;
  }

  public void setNumNewClasses(int i) {
    this.numNewClasses = i;
  }

  public void setNumDeletedClasses(int i) {
    this.numDeletedClasses = i;
  }


  public void setExceptionDuringDiff(Exception exceptionDuringDiff) {
    this.exceptionDuringDiff = exceptionDuringDiff;
  }


  public Exception getExceptionDuringDiff() {
    return exceptionDuringDiff;
  }


  public void setErrorCause(String errorCause) {
    this.errorCause = errorCause;
  }


  public String getErrorCause() {
    return errorCause;
  }

  /**
   * @return the classesWithoutDefinition
   */
  public final int getClassesWithoutDefinition() {
    return classesWithoutDefinition;
  }

  /**
   * @param classesWithoutDefinition the classesWithoutDefinition to set
   */
  public final void setClassesWithoutDefinition(int classesWithoutDefinition) {
    this.classesWithoutDefinition = classesWithoutDefinition;
  }

  /**
   * @return the classesWithoutLabel
   */
  public final int getClassesWithoutLabel() {
    return classesWithoutLabel;
  }

  /**
   * @param classesWithoutLabel the classesWithoutLabel to set
   */
  public final void setClassesWithoutLabel(int classesWithoutLabel) {
    this.classesWithoutLabel = classesWithoutLabel;
  }

  /**
   * @return the classesCount
   */
  public final int getClassesCount() {
    return classesCount;
  }

  /**
   * @param classesCount the classesCount to set
   */
  public final void setClassesCount(int classesCount) {
    this.classesCount = classesCount;
  }

  /**
   * @return the individualsCount
   */
  public final int getIndividualsCount() {
    return individualsCount;
  }

  /**
   * @param individualsCount the individualsCount to set
   */
  public final void setIndividualsCount(int individualsCount) {
    this.individualsCount = individualsCount;
  }

  /**
   * @return the propertiesCount
   */
  public final int getPropertiesCount() {
    return propertiesCount;
  }

  /**
   * @param propertiesCount the propertiesCount to set
   */
  public final void setPropertiesCount(int propertiesCount) {
    this.propertiesCount = propertiesCount;
  }

  /**
   * @return the maxDepth
   */
  public final int getMaxDepth() {
    return maxDepth;
  }

  /**
   * @param maxDepth the maxDepth to set
   */
  public final void setMaxDepth(int maxDepth) {
    this.maxDepth = maxDepth;
  }

  /**
   * @return the maxChildren
   */
  public final int getMaxChildren() {
    return maxChildren;
  }

  /**
   * @param maxChildren the maxChildren to set
   */
  public final void setMaxChildren(int maxChildren) {
    this.maxChildren = maxChildren;
  }

  /**
   * @return the avgChildren
   */
  public final int getAvgChildren() {
    return avgChildren;
  }

  /**
   * @param avgChildren the avgChildren to set
   */
  public final void setAvgChildren(int avgChildren) {
    this.avgChildren = avgChildren;
  }

  /**
   * @return the singleChild
   */
  public final int getSingleChild() {
    return singleChild;
  }

  /**
   * @param singleChild the singleChild to set
   */
  public final void setSingleChild(int singleChild) {
    this.singleChild = singleChild;
  }

  /**
   * @return the more25Children
   */
  public final int getMore25Children() {
    return more25Children;
  }

  /**
   * @param more25Children the more25Children to set
   */
  public final void setMore25Children(int more25Children) {
    this.more25Children = more25Children;
  }

  /**
   * @return the classesWithMore1Parent
   */
  public final int getClassesWithMore1Parent() {
    return classesWithMore1Parent;
  }

  /**
   * @param classesWithMore1Parent the classesWithMore1Parent to set
   */
  public final void setClassesWithMore1Parent(int classesWithMore1Parent) {
    this.classesWithMore1Parent = classesWithMore1Parent;
  }

  /**
   * @return the descriptionLogicName
   */
  public final String getDescriptionLogicName() {
    return descriptionLogicName;
  }

  /**
   * @param descriptionLogicName the descriptionLogicName to set
   */
  public final void setDescriptionLogicName(String descriptionLogicName) {
    this.descriptionLogicName = descriptionLogicName;
  }

  /**
   * @return the numberOfLeaves
   */
  public final int getNumberOfLeaves() {
    return numberOfLeaves;
  }

  /**
   * @param numberOfLeaves the numberOfLeaves to set
   */
  public final void setNumberOfLeaves(int numberOfLeaves) {
    this.numberOfLeaves = numberOfLeaves;
  }

  public String getPreviousVersion() {
    return previousVersion;
  }

  public void setPreviousVersion(String previousVersion) {
    this.previousVersion = previousVersion;
  }

  public String getCurrentVersion() {
    return currentVersion;
  }

  public void setCurrentVersion(String currentVersion) {
    this.currentVersion = currentVersion;
  }
}
