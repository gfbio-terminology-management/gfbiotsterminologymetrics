package org.gfbio.terminologies.metrics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.gfbio.config.ConfigSvc;
import org.gfbio.terminologies.xml.OntologyChangesBean;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.UnloadableImportException;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.semanticweb.owlapi.util.DLExpressivityChecker;

public class GFBioOWLMetricsCalculator extends GFBioMetricsCalculator {

  private static final Logger LOGGER = Logger.getLogger(GFBioOWLMetricsCalculator.class);

  private OWLOntologyManager manager;
  private OWLOntology ontology;

  private ArrayList<String> definitionProperties = new ArrayList<String>();
  private ArrayList<String> labelProperties = new ArrayList<String>();
  private Stream<OWLClass> classesSet;
  private Set<OWLOntology> imports;

  // serves as container for calculated metrics and properties
  private OntologyChangesBean changeBean;

  private ConfigSvc cfgSvc = ConfigSvc.getInstance(null);



  /**
   * Constructor
   * 
   * @param ontology_url file path to the ontology
   * @throws OWLOntologyCreationException
   * @throws UnloadableImportException
   */
  public GFBioOWLMetricsCalculator(String ontology_url, OntologyChangesBean bean) {
    this.changeBean = bean;
    loadOntology(ontology_url);
  }

  /**
   * Method to calculate the metrics for owl files including their imported files.
   */
  public void calculateMetrics() {

    LOGGER.info("complex metrics calculation started ");

    int nodesWithChildren = 0;
    double allChildren = 0;
    // boolean label, definition;

    Set<OWLClass> classes = new HashSet<OWLClass>();

    this.classesSet = ontology.classesInSignature();
    this.classesSet.forEach(cl -> classes.add(cl));

    // Extract DL Expressivity
    // TODO FB 03/02/21 incorrect results?
    DLExpressivityChecker checker = new DLExpressivityChecker(this.imports);
    changeBean.setDescriptionLogicName(checker.getDescriptionLogicName());
    LOGGER.info("DL expressivity: " + changeBean.getDescriptionLogicName());

    int classesWithoutLabel = 0, classesWithoutDefinition = 0, numberOfLeaves = 0,
        classesWithMore1Parent = 0, maxChildren = 0, singleChild = 0, maxDepth = 0,
        more25Children = 0, avgChildren = 0;

    for (OWLClass c : classes) {

      int currentChildrenCount = Math.toIntExact(EntitySearcher.getSubClasses(c, ontology).count());

      if (currentChildrenCount == 0)
        numberOfLeaves++;


      int parents = 0;
      if (EntitySearcher.getSuperClasses(c, ontology).count() > 1) {

        Set<OWLClassExpression> superClasses = new HashSet<OWLClassExpression>();
        EntitySearcher.getSuperClasses(c, ontology).forEach(sc -> superClasses.add(sc));

        for (OWLClassExpression cl : superClasses)
          // Only when a concrete superclass is specified (avoiding class expressions)
          if (cl.getSignature().size() == 1)
            parents++;

        if (parents > 1)
          classesWithMore1Parent++;
      }

      if (currentChildrenCount > 0)
        nodesWithChildren++;


      if (currentChildrenCount > maxChildren)
        maxChildren = currentChildrenCount;

      if (currentChildrenCount == 1)
        singleChild++;

      if (currentChildrenCount >= 25) {
        more25Children++;
      }

      int depth = findDepthOfSubclasses(c);
      if (depth > maxDepth)
        maxDepth = depth;


      allChildren = allChildren + currentChildrenCount;
      // }

      if (nodesWithChildren != 0) {
        avgChildren = (int) Math.round(allChildren / nodesWithChildren);
      }
    }
    changeBean.setClassesWithoutLabel(classesWithoutLabel);
    changeBean.setClassesWithoutDefinition(classesWithoutDefinition);
    changeBean.setNumberOfLeaves(numberOfLeaves);
    changeBean.setMaxDepth(maxDepth);
    changeBean.setMaxChildren(maxChildren);
    changeBean.setClassesWithMore1Parent(classesWithMore1Parent);
    changeBean.setSingleChild(singleChild);
    changeBean.setAvgChildren(avgChildren);
    changeBean.setMore25Children(more25Children);
  }



  /**
   * Method to load the ontology for further processing.
   * 
   * @param ontology_url local path to the ontology file
   */
  private void loadOntology(String ontology_url) {
    manager = OWLManager.createOWLOntologyManager();
    IRI iri = IRI.create("file:///" + ontology_url);
    try {
      ontology = manager.loadOntologyFromOntologyDocument(iri);
      imports = manager.getImportsClosure(ontology);
      // if (ontology != null)
      // this.reasoner = new StructuralReasonerFactory().createReasoner(ontology);

    } catch (UnloadableImportException e) {
      LOGGER.error("loading ontology failed due to " + e.getLocalizedMessage());
      e.printStackTrace();
    } catch (OWLOntologyCreationException e) {
      LOGGER.error("loading ontology failed due to " + e.getLocalizedMessage());
      e.printStackTrace();
    }
  }

  /**
   * Method to recursively calculate the maximum depth of the ontology. Since this is a graph and
   * not a tree checking must be perforemd for every class (node)
   * 
   * @param c The class to begin the descent.
   * @return depth of the graph "under" the class.
   */
  private int findDepthOfSubclasses(OWLClass c) {
    int deepest = 0;
    boolean top = false;

    Set<OWLClassExpression> subClasses = new HashSet<OWLClassExpression>();
    EntitySearcher.getSubClasses(c, ontology).forEach(sc -> subClasses.add(sc));

    for (OWLClassExpression child : subClasses) {
      if (!child.isAnonymous() && child != c)
        deepest = Math.max(deepest, findDepthOfSubclasses(child.asOWLClass()));
      if (child.isTopEntity()) {
        top = true;
      }
    }
    return !top ? deepest + 1 : deepest;
  }

  public void setDefinition(String definitionProperty) {
    if (definitionProperty != null) {
      this.definitionProperties.add(definitionProperty);
      if (!definitionProperty.startsWith("http://")) {
        this.definitionProperties.add(cfgSvc.resolveNamespaceToUri(definitionProperty));
      }
    }
  }

  public void setLabel(String label) {

    LOGGER.info("setting label to " + label);

    if (label != null) {
      this.labelProperties.add(label);
      if (!label.startsWith("http://")) {
        this.labelProperties.add(cfgSvc.resolveNamespaceToUri(label));
      }
    }
  }


  @Override
  public int getClassesCount() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getIndividualsCount() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getPropertiesCount() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getMaxDepth() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getMaxChildren() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getAvgChildren() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getSingleChild() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getMore25Children() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getClassesWithoutDefinition() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getClasesWithoutLabel() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getClassesWithMoreThan1Parent() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getNumberOfLeaves() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public String getDescriptionLogicName() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void setDescriptionLogicName(String descriptionLogicName) {
    // TODO Auto-generated method stub

  }

  @Override
  public void setAcronym(String acronym) {
    // TODO Auto-generated method stub

  }

}
