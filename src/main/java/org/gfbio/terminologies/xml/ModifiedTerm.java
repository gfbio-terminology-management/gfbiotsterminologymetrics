package org.gfbio.terminologies.xml;

import java.util.ArrayList;
import java.util.Objects;
import org.semanticweb.owlapi.model.IRI;

public class ModifiedTerm {

  // Annotation Properties
  private ArrayList<String> deletedAnnotations;
  private ArrayList<String> addedAnnotations;

  // Axioms
  private ArrayList<String> deletedAxioms;
  private ArrayList<String> addedAxioms;

  private IRI termIRI;

  private String termLabel;

  public ModifiedTerm() {
    deletedAnnotations = new ArrayList<String>();
    addedAnnotations = new ArrayList<String>();

    deletedAxioms = new ArrayList<String>();
    addedAxioms = new ArrayList<String>();
  }

  public IRI getIRI() {
    return termIRI;
  }

  public void setIRI(IRI termIRI) {
    this.termIRI = termIRI;
  }

  public void addDeletedAnnotation(String annotation) {
    deletedAnnotations.add(annotation);
  }

  public void addAddedAnnotation(String annotation) {
    addedAnnotations.add(annotation);
  }

  public ArrayList<String> getDeletedAnnotations() {
    return deletedAnnotations;
  }

  public ArrayList<String> getAddedAnnotations() {
    return addedAnnotations;
  }

  public void addDeletedAxiom(String axiom) {
    deletedAxioms.add(axiom);
  }

  public void addAddedAxiom(String axiom) {
    addedAxioms.add(axiom);
  }

  public ArrayList<String> getDeletedAxioms() {
    return deletedAxioms;
  }

  public ArrayList<String> getAddedAxioms() {
    return addedAxioms;
  }

  public String getTermLabel() {
    return termLabel;
  }

  public void setTermLabel(String termLabel) {
    this.termLabel = termLabel;
  }

  @Override
  public int hashCode() {
    return Objects.hash(termIRI, termLabel);
  }

  @Override
  public boolean equals(Object anotherTermIRI) {
    System.out.println(" comparing objects ");
    System.out.println(this.termIRI.toString());
    System.out.println((String) anotherTermIRI);
    System.out.println(this.termIRI.toString().equalsIgnoreCase((String) anotherTermIRI));
    if (this.termIRI.toString().equalsIgnoreCase((String) anotherTermIRI))
      return true;
    else
      return false;
  }


}
