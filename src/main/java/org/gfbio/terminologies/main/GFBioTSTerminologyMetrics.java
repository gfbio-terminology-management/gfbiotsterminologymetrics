package org.gfbio.terminologies.main;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

/**
 * Main entry point
 *
 */
public class GFBioTSTerminologyMetrics {

  private static final Logger LOGGER = Logger.getLogger(GFBioTSTerminologyMetrics.class);

  public static void main(String[] args) {

    // supplied parameters
    String ontoAcronym = null, ontoReleaseDate = null, type = null, pathToIni = null;
    boolean loadImports = false, paramsSet = false;

    Options options = new Options();
    HelpFormatter formatter = new HelpFormatter();

    try {

      // create commandline options
      options.addOption("a", true, "ontology acronym (uppercase)");
      options.addOption("r", true, "release date (YYYY-MM-DD)");
      options.addOption("t", true, "type (owl|ttl)");
      options.addOption("i", true, "load imports (true|false)");
      options.addOption("c", true,
          "absolute path to configuration .ini (default: /var/opt/ts/metrics.ini)");

      // create the parser
      CommandLineParser parser = new DefaultParser();

      // parse the command line arguments
      CommandLine line = parser.parse(options, args);

      // read in options
      if (line.hasOption("a") && line.hasOption("r") && line.hasOption("t")
          && line.hasOption("i")) {

        // initialise the member variable
        ontoAcronym = line.getOptionValue("a");
        ontoReleaseDate = line.getOptionValue("r");
        type = line.getOptionValue("t");
        loadImports = Boolean.valueOf(line.getOptionValue("i"));
        pathToIni =
            line.getOptionValue("c") == null ? "/var/opt/ts/metrics.ini" : line.getOptionValue("c");

        paramsSet = true;
      }

      if (paramsSet == true) {
        LOGGER.info("starting terminology metrics calculation");
        MetricsFacade facade = new MetricsFacade();
        facade.init(ontoAcronym, ontoReleaseDate, type, pathToIni, loadImports);
        facade.run();
      } else {
        LOGGER.warn("Parameter(s) missing");
        formatter.printHelp("java -jar metrics.jar", options, true);
      }

      LOGGER.info("Exiting program now");
      System.exit(0);

    } catch (ParseException exp) {

      LOGGER.error("Parsing failed.  Reason: " + exp.getMessage());

      formatter.printHelp("metrics", options, true);

      System.exit(-1);
    }


  }
}
