package org.gfbio.terminologies.metrics;

import java.net.URI;
import java.util.ArrayList;
import java.util.Set;
import org.gfbio.config.ConfigSvc;
import org.semanticweb.skos.SKOSAnnotation;
import org.semanticweb.skos.SKOSConcept;
import org.semanticweb.skos.SKOSCreationException;
import org.semanticweb.skos.SKOSDataset;
import org.semanticweb.skos.SKOSEntity;
import org.semanticweb.skosapibinding.SKOSManager;

public class GFBioSKOSMetricsCalculator extends GFBioMetricsCalculator {

  SKOSDataset dataset;
  Set<SKOSConcept> concepts;
  private int conceptsCount;
  private int propertiesCount;
  private int maxDepth;
  private int maxChildren;
  private int avgChildren;
  private int singleChild;
  private int more25Children;
  private int classesWithoutDefinition;
  private int classesWithoutLabel;
  private int classesWithMore1Parent;
  private int numberOfLeaves;
  private String definitionProperty;
  private String labelProperty;
  SKOSManager manager;
  private String descriptionLogicName;
  private ArrayList<String> properties;

  private ConfigSvc cfgSvc = ConfigSvc.getInstance(null);

  /**
   * Constructor for using the calculator from somewhere else
   * 
   * @param ontology_url file path to the ontology
   */
  public GFBioSKOSMetricsCalculator(String ontology_url) {
    init();
    loadOntology(ontology_url);
  }

  private void init() {
    this.conceptsCount = 0;
    this.propertiesCount = 0;
    this.maxDepth = 0;
    this.maxChildren = 0;
    this.avgChildren = 0;
    this.singleChild = 0;
    this.more25Children = 0;
    this.classesWithoutDefinition = 0;
    this.classesWithoutLabel = 0;
    this.properties = new ArrayList<String>();
  }

  public void setDefinition(String definitionProperty) {
    if (definitionProperty != null && !definitionProperty.isEmpty()
        && !definitionProperty.startsWith("http://")) {
      this.definitionProperty = cfgSvc.resolveNamespaceToUri(definitionProperty);
    } else if (definitionProperty.startsWith("http://")) {
      this.definitionProperty = definitionProperty;
    } else {
      definitionProperty = null;
    }
  }

  public void setLabel(String label) {
    if (label != null && !label.isEmpty() && !label.startsWith("http://")) {
      this.labelProperty = cfgSvc.resolveNamespaceToUri(label);
    } else if (label.startsWith("http://")) {
      this.labelProperty = label;
    } else {
      this.labelProperty = null;
    }
  }

  /**
   * Method to load the ontology for further processing.
   * 
   * @param ontology_url local path to the ontology file
   */
  private void loadOntology(String ontology_url) {
    try {
      manager = new SKOSManager();
      dataset = manager.loadDataset(URI.create(ontology_url));
      descriptionLogicName = "AL";
    } catch (SKOSCreationException e) {
      e.printStackTrace();
    }
  }

  /**
   * The method to calculate the metrics for SKOS files.
   * 
   * @throws SKOSCreationException
   */
  public void calculateMetrics() {

    concepts = dataset.getSKOSConcepts();
    conceptsCount = concepts.size();
    double all_children = 0;
    int nodes = 0;
    int classesWithDefinition = 0;
    int classesWithLabel = 0;
    for (SKOSConcept c : concepts) {
      int depth = findDepthOfSubclasses(c);
      if (depth > maxDepth)
        maxDepth = depth;
      int current_children = 0;
      boolean hasDefinition = false;
      boolean hasLabel = false;
      int numberOfParents = 0;

      for (SKOSAnnotation assertion : dataset.getSKOSAnnotations(c)) {
        URI prop = assertion.getURI();
        if (prop.getFragment() == null)
          continue;
        if (!properties.contains(prop.getFragment().toString())) {
          properties.add(prop.getFragment().toString());
        }
        if (prop.getFragment().toString().equals("narrower")) {
          current_children++;
        }
        if (!hasDefinition && prop.toString().equals(definitionProperty)) {
          hasDefinition = true;
          classesWithDefinition++;
        }
        if (!hasLabel && this.labelProperty != null && prop.toString().equals(this.labelProperty)) {
          hasLabel = true;
          classesWithLabel++;
        }
        if (prop.getFragment().toString().equals("broader"))
          numberOfParents++;
      }

      if (numberOfParents > 1)
        this.classesWithMore1Parent++;

      if (current_children == 0)
        this.numberOfLeaves++;
      else
        nodes++;

      if (current_children > maxChildren)
        maxChildren = current_children;

      if (current_children == 1)
        singleChild++;

      if (current_children >= 25)
        more25Children++;

      all_children += current_children;
    }
    propertiesCount = properties.size();
    classesWithoutDefinition = conceptsCount - classesWithDefinition;
    classesWithoutLabel = conceptsCount - classesWithLabel;
    if (nodes != 0) {
      avgChildren = (int) Math.round(all_children / nodes);
    }
  }

  /**
   * This method is used to calculate the max depth of a SKOS ontology
   * 
   * @param c the current node from where we want to calculate the underlying depth
   * @return the deepest path under the node
   */
  private int findDepthOfSubclasses(SKOSConcept c) {
    int deepest = 0;
    for (SKOSAnnotation assertion : dataset.getSKOSAnnotations(c)) {
      URI prop = assertion.getURI();
      if (!(prop.getFragment() == null) && prop.getFragment().toString().equals("narrower")) {
        SKOSEntity entity = assertion.getAnnotationValue();
        for (SKOSConcept con : concepts) {
          if (con.getURI().toString().equals(entity.getURI().toString())) {
            deepest = Math.max(deepest, findDepthOfSubclasses(con));
            break;
          }
        }
      }
    }
    return deepest + 1;
  }


  /*
   * Getter methods
   */
  public int getClassesCount() {
    return conceptsCount;
  }

  public int getIndividualsCount() {
    return 0;
  }

  public int getPropertiesCount() {
    return propertiesCount;
  }

  public int getMaxChildren() {
    return maxChildren;
  }

  public int getSingleChild() {
    return singleChild;
  }

  public int getMore25Children() {
    return more25Children;
  }

  public int getMaxDepth() {
    return maxDepth;
  }

  public int getAvgChildren() {
    return avgChildren;
  }

  public int getClassesWithoutDefinition() {
    return this.classesWithoutDefinition;
  }

  public int getClasesWithoutLabel() {
    return this.classesWithoutLabel;
  }

  public int getClassesWithMoreThan1Parent() {
    return this.classesWithMore1Parent;
  }

  public int getNumberOfLeaves() {
    return this.numberOfLeaves;
  }

  public String getDescriptionLogicName() {
    return this.descriptionLogicName;
  }

  public void setDescriptionLogicName(String descriptionLogicName) {
    this.descriptionLogicName = descriptionLogicName;

  }

  @Override
  public void setAcronym(String acronym) {
    // TODO Auto-generated method stub

  }
}
