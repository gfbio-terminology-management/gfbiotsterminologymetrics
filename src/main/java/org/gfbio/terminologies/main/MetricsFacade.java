package org.gfbio.terminologies.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.gfbio.config.ConfigSvc;
import org.gfbio.db.VirtGraphSingleton;
import org.gfbio.terminologies.configuration.GFBioConfiguration;
import org.gfbio.terminologies.metrics.GFBioMetricsCalculator;
import org.gfbio.terminologies.metrics.GFBioOWLMetricsCalculator;
import org.gfbio.terminologies.metrics.SPARQLAdapter;
import org.gfbio.terminologies.upload.GFBioOntologyUploader;
import org.gfbio.terminologies.xml.OntologyChangesBean;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

public class MetricsFacade {
  private static final Logger LOGGER = Logger.getLogger(GFBioTSTerminologyMetrics.class);

  // configuration container
  private GFBioConfiguration config;

  private ConfigSvc configSvc;

  private VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();

  private GFBioOntologyUploader upload;

  private SPARQLAdapter adapter;

  private GFBioMetricsCalculator calculator;

  private OntologyChangesBean changesBean;

  private String ontology_file_path, GRAPHURI, metadataURI;

  private int acronymHash;

  public void init(String acronym, String releaseDate, String type, String pathToIni,
      boolean loadImports) {

    configSvc = ConfigSvc.getInstance(pathToIni);

    GRAPHURI = configSvc.getGraphUri();
    metadataURI = configSvc.getMetadataUri();

    LOGGER.info(" init MetricsFacade with paramters= " + acronym + "; " + releaseDate + "; "
        + loadImports + "; URI=" + GRAPHURI + "; execSPARQL=" + configSvc.isExecSPARQL());

    config = GFBioConfiguration.getInstance();
    config.setOntologyAcronym(acronym);
    config.setReleaseDate(releaseDate);
    config.initProperties();

    upload = new GFBioOntologyUploader(GRAPHURI, loadImports);

    ontology_file_path = config.getRelease_path() + config.getOnto_file_name() + "." + type;

    if (new File(ontology_file_path).isFile() == false) {
      LOGGER.error(ontology_file_path + " is not a valid file or does not exist!");
      LOGGER.error("exiting program now ...");
      System.exit(-1);
    }

    LOGGER.info("processing ontology " + ontology_file_path);

    acronymHash = acronym.hashCode();

    changesBean = new OntologyChangesBean();
    changesBean.setCurrentVersion(releaseDate);

    adapter = new SPARQLAdapter(changesBean);
  }

  /**
   * 
   */
  public void run() {

    Instant start = Instant.now();

    // extracting metadata for current terminology version from Metadata graph
    HashMap<String, String> metadata = extractMetadata();
    changesBean.setPreviousVersion(metadata.get("releaseDate") != null ? metadata.get("releaseDate")
        : metadata.get("creationDate"));

    // since metric calculations happens now "in" the triple store the new terminology versions
    // needs to be uploaded to the triple store. the old versions needs to be backed up.
    // adapter.backupGraph(config.getOntologyAcronym());
    // LOGGER.info("clearing graph before uploading new version");
    // adapter.dropGraph(config.getOntologyAcronym(), "");
    // the new version of the ontology is uploaded to the triple store now - it is initially named
    // <.../xxxNEW>
    if (adapter.graphExists("") == true) {
      // old graph version exists
      uploadGraph(ontology_file_path, config.getOntologyAcronym(), "NEW");
      adapter.setGraphSuffix("NEW");
    } else {
      // no previous version
      uploadGraph(ontology_file_path, config.getOntologyAcronym(), "");
      adapter.setGraphSuffix("");
    }

    // (1) calculate minimal set of metrics and differences between ontology versions via SPARQL
    // queries
    try {
      adapter.calculateMetrics();
      adapter.calculateDifference();
    } catch (Exception e) {
      LOGGER.error("Exception occured during Metrics/Difference calculation");
      LOGGER.error("Rolling back changes and exiting program");
      e.printStackTrace();

      adapter.dropGraph(config.getOntologyAcronym(), "NEW");

      System.exit(-1);
    }

    // (2) if ontology is eligible for complex calculations, then proceed with them now
    // this will happen using the physical ontology file on the disk
    if (config.getComplexMetrics() == true) {
      calculator = new GFBioOWLMetricsCalculator(ontology_file_path, changesBean);
      calculator.calculateMetrics();
    } else
      LOGGER.info("skipped calculating complex metrics");


    if (adapter.graphExists("NEW")) {
      // when everything is done drop the old graph version
      adapter.dropGraph(config.getOntologyAcronym(), "");
      adapter.dropGraph(config.getOntologyAcronym(), "NEW");
      uploadGraph(ontology_file_path, config.getOntologyAcronym(), "");
    }

    // uploadGraph(ontology_file_path, config.getOntologyAcronym(), "");

    deleteMetadata(0);
    updateMetadata(config.getReleaseDate(), config.getLabel_properties(),
        config.getDefinition_properties(), config.getSynonym_properties(), metadata);
    if (Integer
        .valueOf(metadata.get("lastArchiveID") == null ? "0" : metadata.get("lastArchiveID")) > 0)
      archiveOntology(metadata);

    Instant end = Instant.now();
    LOGGER.info("All done! Upload took " + configSvc.formatDuration(Duration.between(start, end)));
  }


  private ResultSet executeSPARQL(VirtuosoQueryExecution vqe, String query) {
    ResultSet rs = null;
    LOGGER.info("Executing SPARQL query: " + query);
    try {
      rs = vqe.execSelect();
    } catch (Exception e) {
      set.close();
    }
    return rs;
  }

  private void uploadGraph(String file, String acronym, String suffix) {
    if (configSvc.isExecSPARQL() == true) {

      LOGGER.info("uploading ontology file " + file);
      int rc = upload.upload(file, acronym, suffix);
      if (rc == 1) {
        LOGGER.info("Upload successfully completed - indexing files");
        upload.index();
      } else {
        LOGGER.warn("Upload failed - aborting calculation!");
        System.exit(rc);
      }
    }
  }

  private void archiveOntology(HashMap<String, String> metadata) {


    String releaseDate = metadata.get("releaseDate") != null ? metadata.get("releaseDate")
        : metadata.get("creationDate");
    String resourceLocator = metadata.get("resourceLocator");

    // mandatory/computable fields
    int versionId = extractIntFromMap(metadata, "lastArchiveID");
    int numberOfClasses = extractIntFromMap(metadata, "numberOfClasses");
    int numberOfIndividuals = extractIntFromMap(metadata, "numberOfIndividuals");
    int numberOfProperties = extractIntFromMap(metadata, "numberOfProperties");
    int classesWithoutDefinition = extractIntFromMap(metadata, "classesWithoutDefinition");
    int classesWithoutLabel = extractIntFromMap(metadata, "classesWithoutLabel");

    // optional fields; only available if terminology supports complex metrics calculation
    int maximumDepth = extractIntFromMap(metadata, "maximumDepth");
    int maximumNumberOfChildren = extractIntFromMap(metadata, "maximumNumberOfChildren");
    int averageNumberOfChildren = extractIntFromMap(metadata, "averageNumberOfChildren");
    int classesWithASingleChild = extractIntFromMap(metadata, "classesWithASingleChild");
    int classesWithMoreThan25Children =
        extractIntFromMap(metadata, "classesWithMoreThan25Children");
    int classesWithMoreThan1Parent = extractIntFromMap(metadata, "classesWithMoreThan1Parent");
    int numberOfLeaves = extractIntFromMap(metadata, "numberOfLeaves");

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date(System.currentTimeMillis());

    StringBuilder queryBuilder = new StringBuilder();

    deleteMetadata(versionId);

    queryBuilder.append("INSERT INTO GRAPH <" + GRAPHURI + "Metadata> { ");
    queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> a <"
        + metadataURI + "ArchivedOntology>.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <"
        + metadataURI + "releaseDate> '"
        + releaseDate.replace("^^http://www.w3.org/2001/XMLSchema#date", "") + "' .");
    queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <"
        + metadataURI + "archivingDate> '" + formatter.format(date) + "' .");
    queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <"
        + metadataURI + "archiveID> '" + versionId + "' .");

    // mandatory/computable fields
    queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId
        + "> omv:numberOfClasses '" + numberOfClasses + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId
        + "> omv:numberOfIndividuals '" + numberOfIndividuals + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId
        + "> omv:numberOfProperties '" + numberOfProperties + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <"
        + metadataURI + "classesWithoutDefinition> '" + classesWithoutDefinition + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <"
        + metadataURI + "classesWithoutLabel> '" + classesWithoutLabel + "'.");

    // optional fields; only available if terminology supports complex metrics calculation
    if (maximumDepth > 0)
      queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <"
          + metadataURI + "maximumDepth> '" + maximumDepth + "'.");
    if (maximumNumberOfChildren > 0)
      queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <"
          + metadataURI + "maximumNumberOfChildren> '" + maximumNumberOfChildren + "'.");
    if (averageNumberOfChildren > 0)
      queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <"
          + metadataURI + "averageNumberOfChildren> '" + averageNumberOfChildren + "'.");
    if (classesWithASingleChild > 0)
      queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <"
          + metadataURI + "classesWithASingleChild> '" + classesWithASingleChild + "'.");
    if (classesWithMoreThan25Children > 0)
      queryBuilder
          .append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <" + metadataURI
              + "classesWithMoreThan25Children> '" + classesWithMoreThan25Children + "'.");
    if (classesWithMoreThan1Parent > 0)
      queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <"
          + metadataURI + "classesWithMoreThan1Parent> '" + classesWithMoreThan1Parent + "'.");
    if (numberOfLeaves > 0)
      queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId + "> <"
          + metadataURI + "numberOfLeaves> '" + numberOfLeaves + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + File.separator + versionId
        + "> omv:resourceLocator '" + resourceLocator + "'.}");

    String query = queryBuilder.toString();
    queryToFile(config.getRelease_path(), "archive", queryBuilder);
    if (configSvc.isExecSPARQL() == true) {
      LOGGER.info("executing ARCHIVE " + query);

      VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
      VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(query, set);
      executeSPARQL(vqe, query);
      vqe.close();
    }
  }

  private int extractIntFromMap(HashMap<String, String> metadata, String key) {
    return metadata.get(key) != null ? Integer.valueOf(metadata.get(key)) : -1;
  }

  /**
   * Extract metadata from old ontology
   */
  private HashMap<String, String> extractMetadata() {
    HashMap<String, String> metadata = new HashMap<String, String>();

    String metadataQuery = "SELECT ?p ?o FROM <" + GRAPHURI + "Metadata> WHERE {<" + GRAPHURI
        + acronymHash + "> ?p ?o}";
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadataQuery, set);
    ResultSet res = executeSPARQL(vqe, metadataQuery);
    String lang = null;
    String graph = null;

    // System.out.println(metadataQuery);
    if (res.hasNext() == true) {
      LOGGER.info("extracting metadata from previous version");

      while (res.hasNext()) {
        QuerySolution result = res.nextSolution();
        lang = result.get("p").asResource().getLocalName();
        graph = result.get("o").toString();

        metadata.put(lang, graph.trim());
        if (lang.equalsIgnoreCase("lastArchiveID"))
          metadata.put("lastArchiveID", String.valueOf(Integer.valueOf(graph) + 1));

        LOGGER.info(lang + " : " + graph);
      }
      // fix for legacy terminologies, which do not provide lastArchiveID attribute yet
      if (metadata.containsKey("lastArchiveID") == false) {
        metadata.put("isLegacy", "true");
        metadata.put("lastArchiveID", "1");
      }

    } else {
      // no previous version available -> read initial metadata from Mongo
      LOGGER.info("no previous version available -> read initial metadata from Parameters graph");

      // FIXME hard-coded uri
      metadataQuery = "SELECT ?p ?o FROM <" + GRAPHURI
          + "Parameters> WHERE {<http://terminologies.gfbio.org/terms/" + acronymHash + "> ?p ?o}";
      set = VirtGraphSingleton.getInstance().getVirtGraph();
      vqe = VirtuosoQueryExecutionFactory.create(metadataQuery, set);
      res = executeSPARQL(vqe, metadataQuery);
      lang = null;
      graph = null;

      while (res.hasNext()) {
        QuerySolution result = res.nextSolution();
        lang = result.get("p").asResource().getLocalName();
        graph = result.get("o").toString();

        metadata.put(lang, graph.trim());

        LOGGER.info(lang + " : " + graph);
      }

      LOGGER.info(metadata.toString());
    }
    vqe.close();
    return metadata;
  }

  /**
   * 
   * @param versionId identifies an archived version of an terminology's metadata
   */
  private void deleteMetadata(int versionId) {
    StringBuilder uri = new StringBuilder().append(GRAPHURI + acronymHash);
    if (versionId > 0)
      uri.append(File.separator + versionId);

    StringBuilder queryBuilder = new StringBuilder();

    // delete latest version in Metadata graph first
    queryBuilder.append(" WITH <" + GRAPHURI + "Metadata>");
    queryBuilder.append(" DELETE { <" + uri.toString() + "> ?p ?o }");
    queryBuilder.append(" WHERE { <" + uri.toString() + "> ?p ?o }");
    String query = queryBuilder.toString();
    queryToFile(config.getRelease_path(), "delete", queryBuilder);

    LOGGER.info("executing DELETE " + query);

    if (configSvc.isExecSPARQL() == true) {
      VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
      VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(query, set);
      executeSPARQL(vqe, query);
      vqe.close();
    }
  }

  private void queryToFile(String path, String query, StringBuilder sb) {
    try {

      BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(path + query + ".sql")));

      // write contents of StringBuffer to a file
      bwr.write(sb.toString());

      // flush the stream
      bwr.flush();

      // close the stream
      bwr.close();
    } catch (IOException e) {
      LOGGER.error("could not write query to file due to " + e.getLocalizedMessage());
    }

    LOGGER.info("query written to file");
  }

  private void updateMetadata(String releaseDate, List<String> labelProperty,
      List<String> definitionProperty, List<String> synonymProperty,
      HashMap<String, String> metadata) {
    LOGGER.info("updating Metadata for <" + GRAPHURI + acronymHash + ">");

    String IRI = metadata.get("URI");
    String name = metadata.get("name");
    String description = metadata.get("description");
    String domain = metadata.get("hasDomain");
    String ontologyLanguage = metadata.get("hasOntologyLanguage");
    int lastArchiveId = Integer
        .valueOf(metadata.get("lastArchiveID") == null ? "0" : metadata.get("lastArchiveID"));


    StringBuilder queryBuilder = new StringBuilder();

    queryBuilder.append("INSERT INTO GRAPH <" + GRAPHURI + "Metadata> { ");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> a omv:Ontology.");
    queryBuilder.append(
        "<" + GRAPHURI + acronymHash + "> omv:acronym '" + config.getOntologyAcronym() + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> omv:URI <" + IRI + ">.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> omv:name '" + name + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> omv:description '" + description + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI + "lastArchiveID> '"
        + lastArchiveId + "' .");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI + "releaseDate> '"
        + releaseDate + "'^^xsd:date .");
    if (labelProperty != null) {
      for (String label : labelProperty)
        queryBuilder
            .append("<" + GRAPHURI + acronymHash + "> <" + metadataURI + "label> <" + label + ">.");
    }

    if (definitionProperty != null) {
      for (String definition : definitionProperty)
        queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI + "definition> <"
            + definition + ">.");
    }

    if (synonymProperty != null) {
      for (String synonym : synonymProperty)
        queryBuilder.append(
            "<" + GRAPHURI + acronymHash + "> <" + metadataURI + "synonym> <" + synonym + ">.");
    }

    queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI + "graph> <" + GRAPHURI
        + config.getOntologyAcronym() + ">.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> omv:numberOfClasses '"
        + changesBean.getClassesCount() + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> omv:numberOfIndividuals '"
        + changesBean.getIndividualsCount() + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> omv:numberOfProperties '"
        + changesBean.getPropertiesCount() + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI
        + "classesWithoutDefinition> '" + changesBean.getClassesWithoutDefinition() + "'.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI
        + "classesWithoutLabel> '" + changesBean.getClassesWithoutLabel() + "'.");

    if (config.getComplexMetrics()) {
      LOGGER.info("calculating complex metrics");
      queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI + "maximumDepth> '"
          + changesBean.getMaxDepth() + "'.");
      queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI
          + "maximumNumberOfChildren> '" + changesBean.getMaxChildren() + "'.");
      queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI
          + "averageNumberOfChildren> '" + changesBean.getAvgChildren() + "'.");
      queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI
          + "classesWithASingleChild> '" + changesBean.getSingleChild() + "'.");
      queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI
          + "classesWithMoreThan25Children> '" + changesBean.getMore25Children() + "'.");
      queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI
          + "classesWithMoreThan1Parent> '" + changesBean.getClassesWithMore1Parent() + "'.");
      queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI + "numberOfLeaves> '"
          + changesBean.getNumberOfLeaves() + "'.");
      queryBuilder.append("<" + GRAPHURI + acronymHash + "> <" + metadataURI
          + "hasDlExpressivity> '" + changesBean.getDescriptionLogicName() + "'.");
    }

    // queryBuilder.append("<" + GRAPHURI + acronymHash + "> owl:versionInfo <" + releaseDate +
    // ">.");
    queryBuilder.append(
        "<" + GRAPHURI + acronymHash + "> omv:hasOntologyLanguage <" + ontologyLanguage + ">.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> omv:hasDomain <" + GRAPHURI + "ontology#"
        + domain.replace(GRAPHURI + "ontology#", "") + ">.");
    queryBuilder.append("<" + GRAPHURI + acronymHash + "> omv:resourceLocator '/ontologies/"
        + config.getOntologyAcronym().toLowerCase() + "/" + releaseDate + "/"
        + config.getOnto_file_name() + ".owl'.}");

    String query = queryBuilder.toString();
    queryToFile(config.getRelease_path(), "update", queryBuilder);

    LOGGER.info("executing UPDATE " + query);

    if (configSvc.isExecSPARQL() == true) {
      VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
      VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(query, set);
      executeSPARQL(vqe, query);
      vqe.close();
    }
  }
}
