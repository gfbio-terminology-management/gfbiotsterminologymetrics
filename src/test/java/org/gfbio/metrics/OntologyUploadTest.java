package org.gfbio.metrics;

import static org.junit.Assert.assertEquals;
import org.gfbio.terminologies.upload.GFBioOntologyUploader;
import org.junit.Test;

public class OntologyUploadTest {

  @Test
  public void testUpload() {
    GFBioOntologyUploader uploader = GFBioOntologyUploader.getInstance();
    uploader.setUriPrefix("http://taxref.mnhn.fr/lod/graph/");

    int rc = uploader.upload("/var/opt/ts/ontologies/taxref/taxrefld_taxonomy_classes.ttl",
        "classes", "");
    assertEquals(1, rc);

    boolean indexed = uploader.index();
    assertEquals(true, indexed);

    rc = uploader.upload("/var/opt/ts/ontologies/taxref/taxrefld_taxonomy_concepts.ttl", "concepts",
        "");
    assertEquals(1, rc);

    indexed = uploader.index();
    assertEquals(true, indexed);
  }
}
