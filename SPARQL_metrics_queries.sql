-- numberOfClasses
SELECT COUNT(?c) AS ?cnt FROM <http://terminologies.gfbio.org/terms/ENVO> 
WHERE {
    ?c rdf:type owl:Class. 
    FILTER (!isBlank(?c)).
}

-- numberOfIndividuals
SELECT COUNT(?inst) AS ?cnt FROM <http://terminologies.gfbio.org/terms/ENVO> 
WHERE { 
    ?inst rdf:type owl:NamedIndividual.
}

-- numberOfProperties
SELECT COUNT(?prop) AS ?cnt FROM <http://terminologies.gfbio.org/terms/ENVO> 
WHERE { 
    ?prop a ?p. 
    FILTER(?p = owl:DatatypeProperty || ?p = owl:ObjectProperty).
}

-- maximumDepth
-- selects root classes
select ?root  {
  #-- Select root classes (classes that have no
  #-- superclasses other than themselves).
  {
    select ?root {
      ?root a owl:Class
      filter not exists {
        ?root rdfs:subClassOf ?superroot 
        filter ( ?root != ?superroot )
      }
    }
  }
  FILTER(!isBlank(?root)) .
  FILTER NOT EXISTS { ?root owl:deprecated ?isDeprecated }.
}

-- maximumNumberOfChildren
-- will be determined when searching maximum depth

-- numberOfLeaves
-- ?? What's a leaf ??

-- averageNumberOfChildren

-- classesWithASingleChild
SELECT COUNT(?cls) AS ?clsCNT FROM <http://terminologies.gfbio.org/terms/FLOPO> 
WHERE 
{ 
    {
        SELECT DISTINCT ?x
        WHERE 
        { 
            ?x rdf:type owl:Class. 
        }
    } 
    ?cls rdfs:subClassOf ?x . 
    FILTER(!isBlank(?cls)).
}

-- classesWithMoreThan1Parent
SELECT COUNT(?superClass) AS ?superClassCNT
WHERE { 
    <> rdfs:subClassOf+ ?superClass .
}

/*

SELECT ?superClass COUNT(?superClass) AS ?parents
WHERE { 
   {
   SELECT ?c WHERE { ?c a owl:Class . FILTER(!isBlank(?c)) }
}
    ?c rdfs:subClassOf+ ?superClass .
    FILTER(!isBlank(?superClass)) .
}
ORDER BY ?c
*/

-- classesWithMoreThan25Children
SELECT COUNT(?subClass) as ?subClassCNT
WHERE { 
    ?subClass rdfs:subClassOf <> .
}
-- then count how many of these queries return equal or greater than 25

-- classesWithoutDefinition
-- numberOfClasses MINUS 
SELECT  COUNT(?s) as ?CNT FROM <http://terminologies.gfbio.org/terms/ENVO>
WHERE {
  ?s a owl:Class. ?s <> ?o . 
}

-- classesWithoutLabel
-- numberOfClasses MINUS 
SELECT  COUNT(?s) as ?CNT FROM <http://terminologies.gfbio.org/terms/ENVO>
WHERE {
  ?s a owl:Class. ?s rdfs:label ?o . 
}

SELECT  ?s 
WHERE {
  ?s a owl:Class. 
   FILTER NOT EXISTS {
      ?s owl:deprecated ?isDeprecated . 
      ?s rdfs:label ?hasLabel . 
   }
}

SELECT COUNT(DISTINCT ?s) FROM <http://terminologies.gfbio.org/terms/PATO> 
WHERE {
  ?s rdf:type owl:Class . 
   FILTER NOT EXISTS {
      ?s <http://purl.obolibrary.org/obo/IAO_0000115> ?hasDefinition . 
   }
   FILTER(!isBlank(?s)).
}