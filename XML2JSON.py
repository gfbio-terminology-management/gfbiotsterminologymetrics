import json
import xmltodict
import sys
import subprocess
import shlex

path_to_xml = sys.argv[1]
json_out=sys.argv[2]
#namespace = sys.argv[3]

print("XML: ", path_to_xml)
print("JSON out: ", json_out)
#print("namespace: ", namespace)

with open(path_to_xml) as xml_file:
    data_dict = xmltodict.parse(xml_file.read())
xml_file.close()

json_data = json.dumps(data_dict)
with open(json_out, "w") as json_file:
        json_file.write(json_data)
json_file.close()

print("conversion done!")
'''
print('starting conversion to RDF/TRIPLES')
# cat ./ontologies/pato/data.json | java -jar json2rdf-jar-with-dependencies.jar https://localhost/ | /home/lefko/tools/apache-jena-3.17.0/bin/riot --formatted=RDF/XML > diff_triples.rdf
#command = ["cat", json_out, "|", "java", "-jar", "json2rdf-jar-with-dependencies.jar", namespace, "|", "/home/lefko/tools/apache-jena-3.17.0/bin/riot", "--formatted=RDF/XML", '>', 'diff_triples.rdf']
command='cat '+json_out+' | java -jar json2rdf-jar-with-dependencies.jar '+namespace+' | /home/lefko/tools/apache-jena-3.17.0/bin/riot --formatted=TURTLE > diff_triples.ttl'
print(''.join(command))

json_to_rdf = subprocess.run(''.join(command),shell=True, capture_output=True, text=True)

print("The exit code was: %d" % json_to_rdf.returncode)
print(json_to_rdf.stdout)
print(json_to_rdf.stderr)
'''